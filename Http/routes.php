<?php

Route::group(['middleware' => ['web']], function () {
    Route::group(array("prefix" => "admin", "as"=>"admin."), function () {
        Route::group(['middleware' => 'auth:dcms'], function () {

            //PRODUCTS
            Route::group(array("prefix" => "products", "as"=>"products."), function () {

                //CATEGORIES
                Route::group(array("prefix" => "categories","as"=>"categories."), function () {
                    Route::get("{id}/copy", array("as"=>"{id}.copy", "uses" => "CategoryController@copy"));
                    Route::any("api/table", array("as"=>"api.table", "uses" => "CategoryController@getDatatable"));
                    Route::any("rebuildTree", array("as"=>"rebuildTree", "uses" => "CategoryController@rebuildTree"));
                });
                Route::resource("categories", "CategoryController");

                //INFORMATION
                Route::group(array("prefix" => "information","as"=>"information."), function () {
                    Route::any("api/table", array("as"=>"api.table", "uses" => "InformationController@getDatatable"));
                    Route::any("api/articlerelationtable/{article_id?}", array("as"=>"api.articlerelationtable", "uses" => "InformationController@getArticleRelationTable"));
                    Route::any("api/productinformationrelation/{productid?}", array("as"=>"api.productinformationrelation.table", "uses" => "InformationController@getProductInformationRelationTable"));
                    Route::any("api/plantrelationtable/{plant_id?}", array("as"=>"api.plantrelationtable", "uses" => "InformationController@getPlantRelationTable"));
                });
                Route::resource("information", "InformationController");

                //API
                Route::group(array("prefix" => "api","as"=>"api."), function () {
                    Route::any("table", array("as"=>"table", "uses" => "ProductExtensionController@getDatatable"));
                    Route::any("tablerow", array("as"=>"tablerow", "uses" => "ProductExtensionController@getTableRow"));
                    Route::get("pim", array("as"=>"pim", "uses" => "ProductExtensionController@json"));
                    Route::get("xls", array("as"=>"xls", "uses" => "ProductExtensionController@json"));
                    Route::any("relation/table/{article_id?}", array("as"=>"relation.table", "uses" => "ProductExtensionController@getRelationDatatable"));
                    Route::any("table/mediarelation/{product_id?}", array("as"=>"table.mediarelation", "uses" => "ProductExtensionController@getMediaRelationTable"));
                    Route::any("table/plantrelation/{product_id?}", array("as"=>"table.plantrelation", "uses" => "ProductExtensionController@getPlantRelationTable"));
                    Route::any("table/productinformationrelation/{product_id?}", array("as"=>"table.productinformationrelation", "uses" => "ProductExtensionController@getProductInformationRelationTable"));
                    Route::post("productdeliveryrow", array("as"=>"productdeliveryrow", "uses" => "ProductExtensionController@getProductDeliveryRow"));
                });
                Route::get("{id}/copy/{country_id?}", array("as"=>"{id}.copy.{country_id}", "uses" => "ProductExtensionController@copy"));

                Route::get('/sendAvailableAgain_to_customers/{product_id}/{country_id}' , array("as"=>"sendAvailableAgain_to_customers" , "uses"=>"ProductExtensionController@testSendAvailableAgain_to_customers") );
            });

            //LABELS
            Route::group(array("prefix" => "labels","as"=>"labels."), function () {
                Route::any("api/table", array("as"=>"api.table", "uses" => "LabelController@getDatatable"));
            });
            Route::resource("labels", "LabelController");

            Route::resource("products", "ProductExtensionController");
        });
    });
});
