<?php
namespace Dcms\Products\Http\Controllers;

use DB;
use Auth;
use View;
use Input;
use Session;
use Redirect;
use Validator;
use DataTables;
use Dcms\Products\Models\UOM;
use App\Models\Shop\OrderStock;
use Dcms\Products\Models\Image;
use Dcms\Products\Models\Price;
use Dcms\Products\Models\Matter;
use Dcms\Products\Models\Carrier;
use Dcms\Products\Models\Product;
use App\Models\API_Authentication;
use Dcms\Products\Models\UOMStack;
use Dcms\Products\Models\Attachment;
use Illuminate\Support\Facades\Mail;
use Dcms\Products\Models\Information;
use Dcms\Core\Models\Countries\Country;
use Dcms\Products\Models\ProductDelivery;
use Dcms\Products\Models\Usageintentdetail;
use Illuminate\Support\Facades\Notification;
use App\Notifications\API_UsersItemAvailable;
use App\Notifications\Customer_ItemAvailable;
use App\Notifications\API_UsersItemOutOfStock;
use Dcms\Products\Http\Controllers\ProductController;
use Dcms\Dcmsarticles\Http\Controllers\ArticleController;

class ProductExtensionController extends ProductController
{
    public function __construct()
    {
        parent::__construct(); //sets the default columnnames, we can extend these doing an array_merge

        $this->productColumNames = array_merge($this->productColumNames, ['matter_id' => 'matter_id', 'usage' => 'usage', 'usage_intent_id' => 'usage_intent_id', 'npk' => 'npk', 'order_quantity' => 'order_quantity', 'order_unit_id' => 'order_unit_id', 'uom_id' => 'uom_id', 'order_stack_id' => 'order_stack_id']);
        $this->productColumnNamesDefaults = array_merge($this->productColumnNamesDefaults, ['new' => '0', 'pro' => '0']);

        $this->informationColumNames = array_merge($this->informationColumNames, ['new' => 'information_new', 'catalogue' => 'information_catalogue', 'composition' => 'information_composition', 'composition_short' => 'information_composition_short', 'guarantee' => 'information_guarantee', 'manual' => 'information_manual', 'out_of_stock' => 'information_out_of_stock', 'period' => 'information_period', 'image' => 'information_image', 'doses' => 'information_doses', 'how_to_use' => 'information_how_to_use', 'usp' => 'information_usp', 'long_composition' => 'long_composition', 'features' => 'features', 'form_tds' => 'form_tds', 'stock_tds' => 'stock_tds', 'tips_tds' => 'tips_tds',  'safety_tds' => 'safety_tds', 'dose_tds' => 'dose_tds', 'identification_tds' => 'identification_tds', 'video_url' => 'video_url', 'content' => 'content', 'period_temp' => 'period_temp', 'period_temp_type'=>'period_temp_type', 'period_type'=>'period_type', 'highpriority' => 'highpriority','megamenu'=>'megamenu']);

        $this->extendgeneralTemplate = 'dcmsproducts::products/templates/data-pages-labels-articles-conditions';
        $this->informationTemplate = 'dcmsproducts::products/templates/information';
    }

    public function getJSONdata()
    {
        $array = [
            'ean' => '123456', 'dcmcode' => '11111111', 'volume' => '5 kg', 'information' => [
                'productname' => 'DCM Start', 'catalogue' => 'pro', 'composition' => 'NPK', 'image' => 'http://www.dcm-info.com/userfiles/image/pro/product/nl/02015.jpg', 'description' => '<ul>
																													<li>formule rijk aan stikstof, ideaal voor vroege voorjaarsbemesting van gazons en grassportvelden</li>
																													<li>voor een snel herstel na de winter, na het verticuteren of na de doorzaai</li>
																													<li>samenstelling met zowel snelwerkende als organische voedingselementen voor verlengde werking</li>
																												</ul>', 'dose' => '200 - 500 kg/ha', 'Period' => '010101010101010',
            ],
        ];

        return json_encode($array);
    }

    //getInformation will overrule the default of vendor dcweb/dcms
    public function getInformation($id = null)
    {
        if (is_null($id)) {
            return DB::connection('project')->table('languages')->select((DB::connection('project')->raw("'0' as new, '' as catalogue, '' as title, '' as description, '' as meta_description, '' as description_short, '' as marketplace_description,  '' as guarantee, '' as manual, '' as out_of_stock,'' as how_to_use, '' as usp, '' as period, NULL as sort_id, (select max(sort_id) from products_information where language_id = languages.id) as maxsort, '' as information_id, '' as id , '' as product_category_id, '' as composition, '' as composition_short, '' as image, '' as doses, '' as long_composition, '' as features, '' as stock_tds, '' as form_tds, '0' as highpriority, '0' as megamenu, 
            
         '' as 'tips_tds' , '' as 'safety_tds' , '' as 'dose_tds', '' as 'identification_tds',
            
            '' as video_url, '' as content , '' as period_temp, '' as period_temp_type, '' as period_type")), 'id as language_id', 'language', 'language_name', 'country')->get();
        } else {
            return DB::connection('project')->select('
														SELECT products_information.language_id ,sort_id, (select max(sort_id) from products_information as X  where X.language_id = products_information.language_id) as maxsort, language, language_name, country, products_information.title, products_information.description, products_information.meta_description,products_information.description_short, products_information.marketplace_description,   products_information.guarantee, products_information.manual, products_information.out_of_stock, products_information.how_to_use,products_information.usp, products_information.period,  products_information.id as information_id, product_category_id, products_information.composition, products_information.composition_short,  products_information.image, products_information.doses, products_information.catalogue, products_information.new, products_information.long_composition, products_information.features, products_information.stock_tds, products_information.form_tds, products_information.tips_tds, products_information.safety_tds, products_information.dose_tds, products_information.identification_tds,
                                                        video_url, content, period_temp, period_temp_type, period_type, case when highpriority is null then 0 else highpriority end as highpriority, case when megamenu is null then 0 else megamenu end as megamenu
														FROM  products
														INNER JOIN products_to_products_information on products.id = products_to_products_information.product_id
														INNER JOIN products_information on products_to_products_information.product_information_id = products_information.id
														INNER JOIN languages on languages.id = products_information.language_id
														WHERE products.id = ?

														UNION
														SELECT languages.id as language_id , 0, (select max(sort_id) from products_information where language_id = languages.id), language, language_name, country, \'\' as title, \'\' as description, \'\' as meta_description, \'\' as description_short,  \'\' as marketplace_description,\'\' as guarantee, \'\' as out_of_stock, \'\' as how_to_use, \'\' as usp, \'\' as information_id , \'\' , \'\' , \'\' , \'\' , \'\' , \'\' , \'\' , \'\' , \'0\', \'\' , \'\' , \'\' , \'\', \'\' , \'\' , \'\' , \'\' , \'\' , \'\' , \'\' , \'\' , \'\', \'0\' , \'0\' 
														FROM languages
														WHERE id NOT IN (SELECT language_id FROM products_information WHERE id IN (SELECT product_information_id FROM products_to_products_information WHERE product_id = ?)) ORDER BY 1 ', [$id, $id]);
        }
    }

    public function getPlantRelationTable($product_id = 0)
    {
        $information_group_id = 0;
        $ProductInformation = Product::with('information')->find($product_id);

        if (isset($ProductInformation->information) && !is_null($ProductInformation->information) && $ProductInformation->information()->count() > 0) {
            foreach ($ProductInformation->information as $I) {
                if (!is_null($I->information_group_id)) {
                    $information_group_id = $I->information_group_id;
                    break;
                }
            }
        }

        $queryA = DB::connection('project')
            ->table('plants as node')
            ->select(
                (DB::connection('project')->raw("
                                node.id,
                                CONCAT( REPEAT( '-', depth ), botanic) AS title,
                                common,
                                languages.country,
                                case when (select count(*) from products_information_group_to_plants where products_information_group_to_plants.plant_id = node.id and information_group_id = '" . $information_group_id . "') > 0 then 1 else 0 end as checked
                                "))
            )

                    ->leftJoin('plants_language', 'node.id', '=', 'plants_language.plant_id')
                    ->leftJoin('languages', 'plants_language.language_id', '=', 'languages.id')
                    ->orderBy('checked', 'DESC');

        $queryB = clone $queryA;
        $queryB->whereRaw('case when (select count(*) from products_information_group_to_plants where products_information_group_to_plants.plant_id = node.id and information_group_id = "' . $information_group_id . '") > 0 then 1 else 0 end = 1');

        if (intval(session('overrule_default_by_language_id')) > 0) {
            $queryA->where('plants_language.language_id', intval(session('overrule_default_by_language_id')));
        }

        return Datatables::queryBuilder($queryB->union($queryA)->orderBy('checked', 'desc'))
                            ->addColumn('radio', function ($model) {
                                return '<input type="checkbox" name="plant_id[]" value="' . $model->id . '" ' . ($model->checked == 1 ? 'checked="checked"' : '') . ' id="chkbox_plant' . $model->id . '" > ';
                            })->addColumn('language', function ($model) {
                                return '<img src="/packages/Dcms/Core/images/flag-' . strtolower($model->country) . '.svg" style="width:16px; height: auto;" alt="">';
                            })
                            ->rawColumns(['radio', 'language'])
                            ->make(true) ;
    }

    public function getProductInformationRelationTable($product_id = 0)
    {
        if ($product_id) {
            $x = Product::find($product_id)->information->first();
            $product_information_group_id = 0;
            if (!is_null($x)) {
                $product_information_group_id = $x->information_group_id;
            }
        } else {
            $product_information_group_id = 0;
        }

        $queryA = DB::connection('project')
            ->table('products_information as x')
            ->select(
                (
                    DB::connection('project')->raw('
                case when (select count(*) from products_information_to_products_information where products_information_to_products_information.information_group_id_tag = x.information_group_id and products_information_to_products_information.information_group_id = "' . $product_information_group_id . '") > 0 then 1 else 0 end as checked,
                catalogue,
                title,
                languages.country,
                information_group_id')
                )
            )
            ->leftJoin('languages', 'x.language_id', '=', 'languages.id')
            ->whereRaw('x.id IN (SELECT product_information_id FROM products_to_products_information WHERE product_id IN ( SELECT id FROM products WHERE online = 1 ) )')
            ->whereNotNull('x.information_group_id')
            ->orderBy('checked', 'DESC');

        $queryB = clone $queryA;
        $queryB->whereRaw('case when (select count(*) from products_information_to_products_information where products_information_to_products_information.information_group_id_tag = x.information_group_id and products_information_to_products_information.information_group_id = "' . $product_information_group_id . '") > 0 then 1 else 0 end  = 1');

        if (intval(session('overrule_default_by_language_id')) > 0) {
            $queryA->where('x.language_id', session('overrule_default_by_language_id'));
        }

        return Datatables::queryBuilder($queryB->union($queryA)->orderBy('checked', 'desc'))
            ->addColumn('radio', function ($model) {
                return '<input type="checkbox" name="information_group_id[]" value="' . $model->information_group_id . '" ' . ($model->checked == 1 ? 'checked="checked"' : '') . ' id="chkbox_' . $model->information_group_id . '" > ';
            })
            ->addColumn('language', function ($model) {
                return '<img src="/packages/Dcms/Core/images/flag-' . strtolower($model->country) . '.svg" style="width:16px; height: auto;" alt="">';
            })
            ->rawColumns(['radio', 'language'])
            ->make(true);
    }

    public static function getProductInformationByLanguage($language_id = null)
    {
        return Information::where('language_id', '=', $language_id)->lists('title', 'id');
    }

    public static function getDropdownProductsInformationByLanguage($language_id = null)
    {
        $dropdownvalues = ArticleController::getArticlesDetailByLanguage($language_id);

        return Form::select('article[' . $language_id . '][]', $dropdownvalues, [3, 4, 5, 6], ['id' => 'articles-' . $language_id, 'multiple' => 'multiple']);
    }

    public function getUOMOptions()
    {
        $oUOM = UOM::orderBy('uom')->get();
        $UOMOptions = [null => ''];

        if (count($oUOM) > 0) {
            foreach ($oUOM as $UOM) {
                if (!empty($UOM->uom_sap)) {
                    $UOMOptions[$UOM->id] = $UOM->uom . '(SAP: ' . $UOM->uom_sap . ')';
                } else {
                    $UOMOptions[$UOM->id] = $UOM->uom;
                }
            }
        }

        return $UOMOptions;
    }

    public function getUOMSAPOptions()
    {
        $oUOM = UOM::orderBy('uom')->get();
        $UOMOptions = [null => ''];

        if (count($oUOM) > 0) {
            foreach ($oUOM as $UOM) {
                if (!empty($UOM->uom_sap)) {
                    $UOMOptions[$UOM->id] = $UOM->uom . '(SAP: ' . $UOM->uom_sap . ')';
                }
            }
        }

        return $UOMOptions;
    }

    public function getUOMStackOptions()
    {
        $oUOMStack = UOMStack::orderBy('uomstack')->get();
        $UOMStackOptions = [null => ''];
        if (count($oUOMStack) > 0) {
            foreach ($oUOMStack as $UOMstack) {
                $UOMStackOptions[$UOMstack->id] = $UOMstack->uomstack;
            }
        }

        return $UOMStackOptions;
    }

    public function getMatterOptions()
    {
        $oMatter = Matter::with([
            'matterdetail' => function ($q) {
                $q->where('language_id', '=', 1)->get(['matter', 'matter_id']);
            },
        ])
            ->get();
        $MatterOptions = [];
        if (count($oMatter) > 0) {
            foreach ($oMatter as $Matter) {
                $MatterOptions[$Matter->id] = $Matter->matterdetail->matter;
            }
        }

        return $MatterOptions;
    }

    public function getUsageIntentOptions($allowNullValue = false)
    {
        $oUsageintent = Usageintentdetail::where('language_id', '=', 11)->orderBy('usage_intent')->get(['usage_intent', 'usage_intent_id']);
        $UsageIntentOptions = [];
        if ($allowNullValue == true) {
            $UsageIntentOptions[null] = '';
        }
        if (count($oUsageintent) > 0) {
            foreach ($oUsageintent as $Usageintent) {
                $UsageIntentOptions[$Usageintent->usage_intent_id] = $Usageintent->usage_intent;
            }
        }

        return $UsageIntentOptions;
    }

    //return the model to fill the form
    public function getExtendedModel($id = null)
    {
        if (is_null($id)) {
            return [
                'Labels' => DB::connection('project')->select('
														SELECT  products_labels.sort_id,products_labels.id, 0 as checked, concat(\'<img src="\',products_labels_language.image,\'" />\'  )/* group_concat( distinct concat(\'<img src="\',products_labels_language.image,\'" />\'  ) SEPARATOR  \'\' ) */ as image
														FROM products_labels
														INNER JOIN products_labels_language on products_labels_language.label_id = products_labels.id
														GROUP BY (products_labels.id)
														ORDER BY products_labels.sort_id'), 'aMatter' => $this->getMatterOptions(), 'aUsageintent' => $this->getUsageIntentOptions(true), 'pageOptionValuesSelected' => [], 'aUOM' => $this->getUOMOptions(), 'aUOMStack' => $this->getUOMStackOptions(), 'aUOM_sap' => $this->getUOMSAPOptions()
            ];
        } else {
            return [
                'Labels' => DB::connection('project')->select('
														(SELECT  products_labels.sort_id,products_labels.id, 1 as checked, concat(\'<img src="\',products_labels_language.image,\'" />\'  ) /* group_concat( distinct concat(\'<img src="\',products_labels_language.image,\'" />\'  ) SEPARATOR  \'\' )*/  as image
														FROM products_labels
														INNER JOIN products_labels_language on products_labels_language.label_id = products_labels.id
														INNER JOIN products_to_products_labels ON products_to_products_labels.label_id = products_labels.id
														WHERE products_to_products_labels.product_id = ?
														GROUP BY (products_labels.id))
														UNION
														(SELECT  products_labels.sort_id,products_labels.id, 0 as checked, concat(\'<img src="\',products_labels_language.image,\'" />\'  ) /*group_concat( distinct concat(\'<img src="\',products_labels_language.image,\'" />\'  )  SEPARATOR  \'\')*/ as image
														FROM products_labels
														INNER JOIN products_labels_language on products_labels_language.label_id = products_labels.id
														WHERE label_id NOT IN (SELECT label_id FROM products_to_products_labels WHERE product_id = ?)
														GROUP BY (products_labels.id))
														ORDER BY 1', [$id, $id]), 'aMatter' => $this->getMatterOptions(), 'aUsageintent' => $this->getUsageIntentOptions(true), 'pageOptionValuesSelected' => $this->setPageOptionValues($this->getSelectedPages($id)), 'aUOM' => $this->getUOMOptions(), 'aUOM_sap' => $this->getUOMSAPOptions(), 'aUOMStack' => $this->getUOMStackOptions()
            ];
        }
    }

    public function getSelectedPages($productid = null)
    {
        //Fetch the first and best information group_id
        $Product = Product::with('information')->find($productid);

        $Information = $Product->information;
        $information_group_id = null;
        $information_id = null;
        $aInformation_id = [];
        $information_id_statement = null;
        if (count($Information) > 0) {
            foreach ($Information as $I) {
                if (!is_null($I->information_group_id)) {
                    $information_id = $I->id;
                    $aInformation_id[] = $I->id;
                    $information_group_id = $I->information_group_id;
                    //break;
                }
            }
        }
        if (count($aInformation_id) > 0) {
            $information_id_statement = 'AND information_id IN (' . implode(',', $aInformation_id) . ')';
        }

        return DB::connection('project')->select('
													SELECT information_group_id, page_id
													FROM pages_to_products_information_group
													WHERE information_group_id = ? AND information_id ' . $information_id_statement, [$information_group_id]);
    }

    public function getProductDeliveryRow()
    {
        $countries = Country::all();
        $carriers = Carrier::all();

        return view('dcmsproducts::products.partials.product_delivery_row')->with([
            'productDelivery' => new ProductDelivery(),
            'countries' => $countries,
            'carriers' => $carriers,
        ])->render();
    }

    public function setPageOptionValues($objselected_pages)
    {
        $pageOptionValuesSelected = [];
        if (count($objselected_pages) > 0) {
            foreach ($objselected_pages as $obj) {
                $pageOptionValuesSelected[$obj->page_id] = $obj->page_id;
            }
        }

        return $pageOptionValuesSelected;
    }

    public function saveProductLabel(Product $Product)
    {
        $DCMProduct = Product::find($Product->id);
        $DCMProduct->labels()->detach();

        if (request()->has('label') && count(request()->get('label')) > 0) {
            foreach (request()->get('label') as $labelid) {
                $DCMProduct->labels()->attach($labelid);
            }
        }
    }

    public function saveProductToPage(Product $Product)
    {
        $input = request()->all();
        $Information = $Product->information;

        if (count($Information) > 0) {
            //PagesToProductsInformation::
            // 1. delete alle pages waar information_group_id = x
            DB::select(DB::raw('DELETE FROM pages_to_products_information_group WHERE information_group_id IN ('.$Information->pluck('information_group_id')->implode(',').') '));
            
            foreach ($Information as $I) {
                if (!is_null($I->information_group_id)) {
                    // 2. only tag the pages with the same language_id as the information_id
                    if (isset($input['page_id']) && count($input['page_id']) > 0) {
                        foreach ($input['page_id'] as $language_id => $thepages) {
                            if ($language_id == $I->language_id) {
                                foreach ($thepages as $p => $page_id) {
                                    $I->pages()->attach($page_id, ['information_group_id' => $I->information_group_id]);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function saveProductToArticle(Product $Product)
    {
        $input = request()->all();
        $Information = $Product->information;

        if (count($Information) > 0) {
            foreach ($Information as $I) {
                if (!is_null($I->information_group_id)) {
                    $I->articles()->detach();

                    if (isset($input['article_id']) && count($input['article_id']) > 0) {
                        foreach ($input['article_id'] as $i => $article_id) {
                            $I->articles()->attach($article_id, ['information_group_id' => $I->information_group_id]);
                        }
                        break;
                    }
                }
            }
        }
    }

    public function saveProductToCondition($Product = null)
    {
        $condition_ids = request()->has('condition_id') ? request()->get('condition_id') : [];
        if (!is_null($Product->information->first())) {
            $Product->information->first()->conditions()->sync($condition_ids);
        }
    }

    public function saveProductToPlant(Product $Product)
    {
        $input = request()->all();
        $Information = $Product->information;

        if (count($Information) > 0) {
            foreach ($Information as $I) {
                if (!is_null($I->information_group_id)) {
                    $I->plants()->detach();

                    if (isset($input['plant_id']) && count($input['plant_id']) > 0) {
                        foreach ($input['plant_id'] as $i => $plant_id) {
                            $I->plants()->attach($plant_id, ['information_group_id' => $I->information_group_id]);
                        }
                        break;
                    }
                }
            }
        }
    }

    public function saveProductDeliveries(Product $Product)
    {
        if (request()->has('removed_product_deliveries')) {
            ProductDelivery::destroy(request()->get('removed_product_deliveries'));
        }

        $statusOutOfStock = [1, 2, 4, 5];
        $statusInStock = [0, 3];
        if (request()->has('product_deliveries')) {
            foreach (request()->get('product_deliveries') as $product_delivery_id => $data) {
                $product_delivery = ProductDelivery::find($product_delivery_id);
                $product_delivery = $product_delivery ?: new ProductDelivery();

                $initial_message = $product_delivery->message;
                $product_delivery->product_id = $Product->id;
                $product_delivery->country_id = $data['country_id'];
                $product_delivery->carrier_id = $data['carrier_id'];
                $product_delivery->webshop = array_key_exists('webshop', $data);
                $product_delivery->message = $data['message'];
                $product_delivery->pickup = array_key_exists('pickup', $data);
                $product_delivery->delivery = array_key_exists('delivery', $data);
                $product_delivery->occupation = floatval(str_replace(',', '.', $data['occupation']));
                $product_delivery->save();

                if (in_array($initial_message, $statusOutOfStock) && in_array($data['message'], $statusOutOfStock) || in_array($initial_message, $statusInStock) && in_array($data['message'], $statusInStock)) {
                    // no actual change
                } elseif (in_array($initial_message, $statusOutOfStock) && in_array($data['message'], $statusInStock)) {
                    $this->sendAvailableAgain_to_API_partners($Product, $data['country_id']);
                    $this->sendAvailableAgain_to_customers($Product, $data['country_id']);
                } else {
                    $this->sendOutOfStock_to_API_partners($Product, $data['country_id']);
                }
            }
        }
    }

    public function testSendOutOfStock_to_API_partners($product_id, $country_id)
    {
        $product = Product::find($product_id);

        self::sendOutOfStock_to_API_partners($product, $country_id);
        return 'ok - test is out';
    }

    public function sendOutOfStock_to_API_partners(Product $Product, $country_id)
    {
        $data = ['product' => $Product];
        // notify the users based on the message
        $API_users = API_Authentication::where('test', 0)->where('country_id', $country_id)->whereNotNull('notify_email')->get();
        if (!is_null($API_users)) {
            foreach ($API_users as $user) {
                foreach (explode(';', str_replace(',', ';', $user->notify_email)) as $singleEmail) {
                    if (env('APP_ENV') == 'test') {
                        $singleEmail = 'bre@groupdc.be';
                    }
                    Notification::route('mail', $singleEmail)->notify(new API_UsersItemOutOfStock($Product));
                }
            }
        }
        //Send a fixed notification to ourself for monitoring
        Notification::route('mail', 'bre@groupdc.be')->notify(new API_UsersItemOutOfStock($Product));
        Notification::route('mail', 'nke@dcm-info.com')->notify(new API_UsersItemOutOfStock($Product));
    }

    public function testSendAvailableAgain_to_API_partners($product_id, $country_id)
    {
        $product = Product::find($product_id);

        self::sendAvailableAgain_to_API_partners($product, $country_id);
        return 'ok - test is out';
    }

    public function sendAvailableAgain_to_API_partners(Product $Product, $country_id)
    {
        $data = ['product' => $Product];
        // notify the users based on the message
        $API_users = API_Authentication::where('test', 0)->where('country_id', $country_id)->whereNotNull('notify_email')->get();
        if (!is_null($API_users)) {
            foreach ($API_users as $user) {
                foreach (explode(';', str_replace(',', ';', $user->notify_email)) as $singleEmail) {
                    if (env('APP_ENV') == 'test') {
                        $singleEmail = 'bre@groupdc.be';
                    }
                    Notification::route('mail', $singleEmail)->notify(new API_UsersItemAvailable($Product));
                }
            }
        }
        //Send a fixed notification to ourself for monitoring
        Notification::route('mail', 'bre@groupdc.be')->notify(new API_UsersItemAvailable($Product));
        Notification::route('mail', 'nke@dcm-info.com')->notify(new API_UsersItemAvailable($Product));
    }

    public function testSendAvailableAgain_to_customers($product_id, $country_id)
    {
        $product = Product::find($product_id);

        self::sendAvailableAgain_to_customers($product, $country_id);
        return 'ok - test is out';
    }

    public function sendAvailableAgain_to_customers(Product $Product, $country_id)
    {
        $data = ['product' => $Product];
        // notify the users based on the message
        $customers_onHold = OrderStock::where('isNotified', 0)->where('country_id', $country_id)->where('product_ids', 'LIKE', '%,' . $Product->id . ',%')->get();
        if (!is_null($customers_onHold)) {
            foreach ($customers_onHold as $user) {
                foreach (explode(';', str_replace(',', ';', $user->email)) as $singleEmail) {
                    Notification::route('mail', trim($singleEmail))->notify(new Customer_ItemAvailable($Product, $user->locale, !is_null($user->voucher) ? true : false));
                    $user->isNotified = 1;
                    $user->save();
                    // the database will no longer hold isNotified = 1 records since these will be deleted on the fly #DWB2C-94
                }
            }
        }
        OrderStock::where('isNotified', 1)->delete();
        //Send a fixed notification to ourself for monitoring
        Notification::route('mail', 'bre@groupdc.be')->notify(new Customer_ItemAvailable($Product, 'fr-BE', false));
        Notification::route('mail', 'bre@groupdc.be')->notify(new Customer_ItemAvailable($Product, 'nl-BE', true));
        Notification::route('mail', 'bre@groupdc.be')->notify(new Customer_ItemAvailable($Product, 'nl-BE', false));
    }

    public function saveProductInformationToProductInformation(Product $Product)
    {
        $product_information = $Product->information->first();
        if (!is_null($product_information)) {
            $information_group_ids = request()->has('information_group_id') ? array_unique(request()->get('information_group_id')) : [];
            $product_information->productinformations()->sync($information_group_ids);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->validateProductForm() === true) {
            $Product = $this->saveProductProperties();
            $this->saveProductInformation($Product);
            //update model relations
            $Product->refresh();
            $this->saveProductPrice($Product);
            $this->saveProductAttachments($Product);
            $this->saveProductImages($Product);
            $this->saveProductLabel($Product);
            $this->saveProductToPage($Product);
            $this->saveProductToArticle($Product);
            $this->saveProductToPlant($Product);
            $this->saveProductToCondition($Product);
            $this->saveProductDeliveries($Product);
            $this->saveProductInformationToProductInformation($Product);

            // redirect
            Session::flash('message', 'Successfully created Product!');

            return Redirect::to('admin/products');
        } else {
            return $this->validateProductForm();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        if (parent::validateProductForm() === true) {
            $Product = $this->saveProductProperties($id);
            $this->saveProductInformation($Product);
            //update model relations
            $Product->refresh();
            $this->saveProductPrice($Product);
            $this->saveProductAttachments($Product);
            $this->saveProductImages($Product);
            $this->saveProductLabel($Product);
            $this->saveProductToPage($Product);
            $this->saveProductToArticle($Product);
            $this->saveProductToPlant($Product);
            $this->saveProductToCondition($Product);
            $this->saveProductDeliveries($Product);
            $this->saveProductInformationToProductInformation($Product);

            // redirect
            Session::flash('message', 'Successfully updated Product!');
            return Redirect::to('admin/products');
        } else {
            return $this->validateProductForm();
        }
    }

    /**
     * return the requested json data.
     *
     * @return json data
     */
    public function json()
    {
        $term = request()->get('term');
        $language_id = intval(request()->get('language'));

        $pData = Information::select('*',  'title as label')->where('title', 'LIKE', '%' . $term . '%')->where('language_id', '=', $language_id)->get()->toJson();

        return $pData;
    }

    /**
     * copy the model
     *
     * @param  int $product_id
     * @param  int $country_id //helps limiting the prices copy - we don't need the copied product in all countries..
     *
     * @return Response
     */
    public function copy($product_id, $country_id = 0)
    {
        //COPY THE Product
        $Newproduct = Product::find($product_id)->replicate();
        $Newproduct->save();

        //COPY THE Information
        $ProductsInformation = Product::with('Information')->where('id', '=', $product_id)->get();

        if (!is_null($ProductsInformation)) {
            foreach ($ProductsInformation as $P) {
                foreach ($P->Information as $pInformation) {
                    $Newproduct->information()->attach($pInformation->id);
                }
            }
        }

        //COPY THE Attachment
        $Attachements = Attachment::where('product_id', '=', $product_id)->get();
        if (!is_null($Attachements)) {
            foreach ($Attachements as $A) {
                $newAttachement = $A->replicate();
                $newAttachement->product_id = $Newproduct->id;
                $newAttachement->save();
            }
        }

        //COPY THE Attachment
        $Images = Image::where('product_id', '=', $product_id)->get();
        if (!is_null($Images)) {
            foreach ($Images as $Image) {
                $newImage = $Image->replicate();
                $newImage->product_id = $Newproduct->id;
                $newImage->save();
            }
        }

        //COPY THE Prices
        $Prices = Price::where('product_id', '=', $product_id)->where('country_id', '=', $country_id)->get();
        if (!is_null($Prices)) {
            foreach ($Prices as $P) {
                $newPrice = $P->replicate();
                $newPrice->product_id = $Newproduct->id;
                $newPrice->save();
            }
        }

        //COPY THE Label
        $DCMProduct = Product::with('labels')->where('id', '=', $product_id)->get();
        if (!is_null($DCMProduct)) {
            foreach ($DCMProduct as $P) {
                foreach ($P->Labels as $pLabel) {
                    $Newproduct->labels()->attach($pLabel->id);
                }
            }
        }

        return Redirect::to('admin/products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        if (request()->get('table') == 'product') {
            $Product = Product::find($id);
            $Product->labels()->detach();
        }

        parent::destroy($id);

        Session::flash('message', 'Successfully deleted the Product!');

        return Redirect::to('admin/products');
    }
}
