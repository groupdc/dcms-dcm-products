<?php

namespace Dcms\Products\Http\Controllers;

use DB;

use Auth;
use View;
use Input;
use Session;
use Redirect;
use Validator;
use DataTables;
use Illuminate\Support\Str;

use Dcms\Products\Models\Image;
use Dcms\Products\Models\Price;
use Dcms\Products\Models\Carrier;
use Dcms\Products\Models\Product;
use Dcms\Products\Models\Category;
use App\Http\Controllers\Controller;
use Dcms\Products\Models\Attachment;
use Dcms\Products\Models\Information;
use Dcms\Core\Models\Countries\Country;
use Dcms\Products\Models\ProductCarrier;
use Dcms\Products\Models\ProductOptions;
use Dcms\Products\Models\ProductToOptions;

class ProductController extends Controller
{
    public $informatationColumNames = [];
    public $informatationColumNamesDefaults = []; // TO DO - the input on the information tab are array based
    public $productColumNames = [];
    public $productColumnNamesDefaults = []; // e.g. checkboxes left blank will result in NULL database value, if this is not what you want, you can set e.g. array('checkbox_name'=>'0');
    public $extendgeneralTemplate = "";
    public $informationTemplate = "";
    public $information_group_id = "";

    public function __construct()
    {
        $this->middleware('permission:products-browse')->only('index');
        $this->middleware('permission:products-add')->only(['create', 'store']);
        $this->middleware('permission:products-edit')->only(['edit', 'update']);
        $this->middleware('permission:products-delete')->only('destroy');

        $this->informationColumNames = [
            'title'                 => 'information_name'
            , 'description'         => 'information_description'
            , 'description_short'   => 'information_description_short'
            , 'meta_description'    =>  'information_meta_description'
            , 'marketplace_description'    =>  'marketplace_description'
            , 'sort_id'             => 'information_sort_id'
            , 'product_category_id' => 'information_category_id'
            , 'url_slug'            => 'information_name',
        ];

        $this->productColumNames = [
            'online'           => 'online'
            , 'code'           => 'code'
            , 'eancode'        => 'eancode'
            , 'image'          => 'image'
            , 'volume'         => 'volume'
            , 'volume_unit_id' => 'volume_unit_id'
            , 'new'            => 'new'
        ];

        $this->productColumNames = array_merge($this->productColumNames, array('discontinued'=>'discontinued','matter_id'=>'matter_id', 'usage'=>'usage', 'usage_intent_id'=>'usage_intent_id','npk'=>'npk','order_quantity'=>'order_quantity','order_unit_id'=>'order_unit_id','order_stack_id'=>'order_stack_id','admission_region'=>'admission_region','admission_nr'=>'admission_nr','brick'=>'brick','width'=>'width','height'=>'height','depth'=>'depth'));
        $this->productColumnNamesDefaults = array_merge($this->productColumnNamesDefaults, array('new'=>'0','pro'=>'0','discontinued'=>'0'));

        $this->informationColumNames = array_merge($this->informationColumNames, array( 'new'=>'information_new', 'catalogue'=>'information_catalogue', 'composition'=>'information_composition' ,'composition_short'=>'information_composition_short' , 'guarantee'=>'information_guarantee',  'manual'=>'information_manual', 'period'=>'information_period', 'image'=>'information_image', 'doses'=>'information_doses', 'highpriority'=>'highpriority', 'megamenu'=>'megamenu'));
       
        $this->extendgeneralTemplate = null;
        $this->informationTemplate = null;
        $this->information_group_id = null;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return View::make('dcmsproducts::products/index');
    }

    /**
     * $priceRows contains an array of Price-Models
     *
     * @return the row to inject prices
     */
    public function getPriceRow($priceRows=array(), $forceEmpty = false)
    {
        $rowstring = "";

        $openbody = true;
        $closebody = true;
        if ($forceEmpty === true && empty($priceRows) === true) {
            $openbody = false;
            $closebody = false;
            $priceRows[] = (object) array();
        }

        foreach ($priceRows as $Price) {
            $pricetype_option = "";
            foreach (['retail-price','purchase-price','purchase-price-UOM','purchase-price-1UT','purchase-price-3UT','purchase-price-6UT','purchase-price-14UT','purchase-price-24UT','purchase-price-28UT', 'next season price' ] as $pricetypes => $pricetype) {
                $selected = "";
                if (isset($Price->price_type) && $pricetype ==$Price->price_type) {
                    $selected = "selected";
                }

                $pricetype_option .= '<option value="'.$pricetype.'" '.$selected.'>'.$pricetype.'</option>';
            }

            $country_option = "";

            $onlineChecked = "";
            if (isset($Price->online) && $Price->online == 1) {
                $onlineChecked = "checked";
            }

            foreach ($this->getCountries("array") as $countryid => $country) {
                $selected = "";
                if (isset($Price->country_id) && $countryid == $Price->country_id) {
                    $selected = "selected";
                }

                $country_option .= '<option value="' . $countryid . '" ' . $selected . '>' . $country . '</option>';
            }

            $tax_option = "";
            foreach ($this->getTaxClasses("array") as $taxid => $tax) {
                $selected = "";
                if (isset($Price->price_tax_id) && $taxid == $Price->price_tax_id) {
                    $selected = "selected";
                }

                $tax_option .= '<option value="'.$taxid.'" '.$selected.' >'.$tax.'</option>';
            }

            if ($openbody === true) {
                $rowstring .= '<tbody >';
            }

            //------------------------------------------------------------------------
            // 							TEMPLATE FOR THE PRICE ROW
            // 		the {INDEX} tag will be replaced in the form.blade, and this script to the attachment database id - or some text to identify its new
            //------------------------------------------------------------------------
            $rowstring .= ' <tr>
								<td>
									<select id="price-country-id[{INDEX}]" class="form-control" name="price-country-id[{INDEX}]">
										'.$country_option.'
									</select>
								</td>
								<td>
									<select id="price_type[{INDEX}]" class="form-control" name="price_type[{INDEX}]">
										'.$pricetype_option.'
									</select>
								</td>
								<td>
									<input id="price[{INDEX}]" name="price[{INDEX}]" class="form-control" type="text" value="'.(isset($Price->price)?$Price->price:"").'">
								</td>
								<td>
									<select id="price_valuta_id[{INDEX}]" name="price_valuta_id[{INDEX}]" class="form-control">
										<option value="1">euro</option>
									</select>
								</td>
								<td>
									<select id="price_tax_id[{INDEX}]" name="price_tax_id[{INDEX}]" class="form-control">
										' . $tax_option . '
									</select>
								</td>
								<td>
									<input type="checkbox" value="1" name="price_online[{INDEX}]" '.$onlineChecked.' />
								</td>
								<td><a class="btn btn-default pull-right delete-table-row" href=""><i class="far fa-trash-alt"></i></a></td>
							</tr>';

            if (isset($Price->id) && intval($Price->id) > 0) {
                $rowstring = str_replace("{INDEX}", $Price->id, $rowstring);
            }
            $openbody = false;
        }

        if ($closebody === true) {
            $rowstring .= '</tbody>';
        }

        return $rowstring;
    }

    /**
     * $mDefaults contains an array of Price-Models
     *
     * @return the row to inject prices
     */
    public function getAttachmentsRow($mDefaults = [], $forceEmpty = false, $languageToArray = false, $selectedLanguage_id = null)
    {
        $aRowString = [];
        $rowstring = "";

        $encloseBody = true;

        if ($forceEmpty === true && empty($mDefaults) === true) {
            $encloseBody = false;
            $mDefaults[] = (object) [];
        }

        foreach ($mDefaults as $Attachment) {
            $countrieslangauge_option = "";
            foreach ($this->getCountriesLanguages("array") as $language_id => $language_COUNTRY) {
                $selected = "";
                if (isset($Attachment->language_id) && $language_id == $Attachment->language_id) {
                    $selected = "selected";
                } elseif (isset($selectedLanguage_id) && !is_null($selectedLanguage_id) && $language_id == $selectedLanguage_id) {
                    $selected = "selected";
                }

                $countrieslangauge_option .= '<option value="' . $language_id . '" ' . $selected . ' class="optie ' . $selectedLanguage_id . '">' . $language_COUNTRY . '</option>';
            }

            $setTheLanguageID = null;
            if (isset($Attachment->language_id)) {
                $setTheLanguageID = $Attachment->language_id;
            } elseif (isset($selectedLanguage_id) && !is_null($selectedLanguage_id)) {
                $setTheLanguageID = $selectedLanguage_id;
            }

            //------------------------------------------------------------------------
            // 							TEMPLATE FOR THE PRICE ROW
            // 		the {INDEX} tag will be replaced in the form.blade, and this script to the attachment database id - or some text to identify its new
            //------------------------------------------------------------------------
            $rowstring .= ' <tr>
								<td>
									<div class="input-group">
										<input type="hidden" id="attachment-language-id[{INDEX}]" class="form-control" name="attachment-language-id[{INDEX}]" value="' . $setTheLanguageID . '">
										<input type="text" name="attachment-file[{INDEX}]" value="' . (isset($Attachment->file) ? $Attachment->file : "") . '" class="form-control" id="attachmentfile{INDEX}" />
										<span class="input-group-btn">
											<button class="btn btn-primary browse-server-files" id="browse_attachmentfile{INDEX}" type="button">Browse Files</button>
										</span>
									</div>
								</td>
								</td>
								<td>
									<input id="attachmentfilename[{INDEX}]" name="attachment-filename[{INDEX}]" class="form-control" type="text" value="' . (isset($Attachment->filename) ? $Attachment->filename : "") . '">
								</td>
								<td>
									<input id="attachmentproposed[{INDEX}]" name="attachment-proposed[{INDEX}]" class="form-control" type="checkbox" value="1" '.(isset($Attachment->proposed) && $Attachment->proposed == 1  ? 'checked' : "") .'>
								</td>
								<td><a class="btn btn-default pull-right delete-table-row" href=""><i class="far fa-trash-alt"></i></a></td>
							</tr>';

            if (isset($Attachment->id) && intval($Attachment->id) > 0) {
                $rowstring = str_replace("{INDEX}", $Attachment->id, $rowstring);
            }

            if ($languageToArray) {
                if (!isset($aRowString[$Attachment->language_id])) {
                    $aRowString[$Attachment->language_id] = "";
                }

                $aRowString[$Attachment->language_id] .= $rowstring;
                $rowstring = "";
            }
        }
        if ($encloseBody === true) {
            $rowstring = '<tbody>' . $rowstring . '</tbody>';
        }

        if ($languageToArray == true && $encloseBody == true) {
            foreach ($aRowString as $langid => $data) {
                $aRowString[$langid] = '<tbody>' . $data . '</tbody>';
            }
        }

        if ($languageToArray == true) {
            return $aRowString;
        } else {
            return $rowstring;
        }
    }
    
    
    /**
     * $mDefaults contains an array of Price-Models
     *
     * @return the row to inject prices
     */
    public function getImagesRow($mDefaults = [], $forceEmpty = false, $languageToArray = false, $selectedLanguage_id = null)
    {
        $aRowString = [];
        $rowstring = "";

        $encloseBody = true;

        if ($forceEmpty === true && empty($mDefaults) === true) {
            $encloseBody = false;
            $mDefaults[] = (object) [];
        }

        foreach ($mDefaults as $Image) {
            $countrieslangauge_option = "";
            foreach ($this->getCountriesLanguages("array") as $language_id => $language_COUNTRY) {
                $selected = "";
                if (isset($Image->language_id) && $language_id == $Image->language_id) {
                    $selected = "selected";
                } elseif (isset($selectedLanguage_id) && !is_null($selectedLanguage_id) && $language_id == $selectedLanguage_id) {
                    $selected = "selected";
                }
                $countrieslangauge_option .= '<option value="' . $language_id . '" ' . $selected . ' class="optie ' . $selectedLanguage_id . '">' . $language_COUNTRY . '</option>';
            }

            $setTheLanguageID = null;
            if (isset($Image->language_id)) {
                $setTheLanguageID = $Image->language_id;
            } elseif (isset($selectedLanguage_id) && !is_null($selectedLanguage_id)) {
                $setTheLanguageID = $selectedLanguage_id;
            }

            //------------------------------------------------------------------------
            // 							TEMPLATE FOR THE PRICE ROW
            // 		the {INDEX} tag will be replaced in the form.blade, and this script to the image database id - or some text to identify its new
            //------------------------------------------------------------------------
            $rowstring .= ' <tr>
								<td>
									<div class="input-group">
										<input type="hidden" id="image-language-id[{INDEX}]" class="form-control" name="image-language-id[{INDEX}]" value="' . $setTheLanguageID . '">
										<input type="text" name="image-file[{INDEX}]" value="' . (isset($Image->file) ? $Image->file : "") . '" class="form-control" id="imagefile{INDEX}" />
										<span class="input-group-btn">
											<button class="btn btn-primary browse-server" id="browse_imagefile{INDEX}" type="button">Browse Files</button>
										</span>
									</div>
								</td>
								</td>
								<td>
                                    <select id="imagefilename[{INDEX}]" name="image-filename[{INDEX}]" class="form-control">
                                    
                                        <option value="front" ' . ((isset($Image->filename) && $Image->filename=="front") ? "selected" : "").'>front</option>
                                        <option value="back" ' . ((isset($Image->filename) && $Image->filename=="back") ? "selected" : "").'>back</option>
                                        <option value="left" ' . ((isset($Image->filename) && $Image->filename=="left") ? "selected" : "").'>left</option>
                                        <option value="right" ' . ((isset($Image->filename) && $Image->filename=="right") ? "selected" : "").'>right</option>
                                        <option value="content" ' . ((isset($Image->filename) && $Image->filename=="content") ? "selected" : "").'>content</option>
                                        <option value="other" ' . ((isset($Image->filename) && $Image->filename=="other") ? "selected" : "").'>other</option>

                                    </select>
								</td>
								<td>
									<input id="imageproposed[{INDEX}]" name="image-proposed[{INDEX}]" class="form-control" type="checkbox" value="1" '.(isset($Image->proposed) && $Image->proposed == 1  ? 'checked' : "") .'>
								</td>
								<td>
									<input id="imageonline[{INDEX}]" name="image-online[{INDEX}]" class="form-control" type="checkbox" value="1" '.(isset($Image->online) && $Image->online == 1  ? 'checked' : "") .'>
								</td>
								<td><a class="btn btn-default pull-right delete-table-row" href=""><i class="far fa-trash-alt"></i></a></td>
							</tr>';

            if (isset($Image->id) && intval($Image->id) > 0) {
                $rowstring = str_replace("{INDEX}", $Image->id, $rowstring);
            }

            if ($languageToArray) {
                if (!isset($aRowString[$Image->language_id])) {
                    $aRowString[$Image->language_id] = "";
                }

                $aRowString[$Image->language_id] .= $rowstring;
                $rowstring = "";
            }
        }
        if ($encloseBody === true) {
            $rowstring = '<tbody>' . $rowstring . '</tbody>';
        }

        if ($languageToArray == true && $encloseBody == true) {
            foreach ($aRowString as $langid => $data) {
                $aRowString[$langid] = '<tbody>' . $data . '</tbody>';
            }
        }

        if ($languageToArray == true) {
            return $aRowString;
        } else {
            return $rowstring;
        }
    }

    public function getTableRow()
    {
        if (request()->get("data") === "price") {
            return $this->getPriceRow(null, true);
        } elseif (request()->get("data") === "attachments") {
            $language_id = null;
            if (request()->has('language_id')) {
                $language_id = request()->get('language_id');
            }

            return $this->getAttachmentsRow(null, true, false, $language_id);
        } elseif (request()->get("data") === "images") {
            $language_id = null;
            if (request()->has('language_id')) {
                $language_id = request()->get('language_id');
            }

            return $this->getImagesRow(null, true, false, $language_id);
        }
    }

    /**
     * return the requested json data.
     *
     * @return json data
     */
    public function json()
    {
        $term = request()->get("term");

        //the json autoload tool needs some
        if (request()->has("language")) {
            $language_id = intval(request()->get("language"));
            $pData = Information::select('id', 'title as label', 'meta_description', 'description', 'description_short')->where('title', 'LIKE', '%' . $term . '%')->where('language_id', '=', $language_id)->get()->toJson();
        } else {
            $language_id = intval(request()->get("language"));
            $pData = Information::select('id', 'title as label', 'meta_description', 'description', 'description_short')->where('title', 'LIKE', '%' . $term . '%')->get()->toJson();
        }

        return $pData;
    }

    /**
     * get the data for DataTable JS plugin.
     *
     * @return Response
     */
    public function getDatatable()
    {
        $query = DB::connection("project")->table("products")->select(
            "products.id",
            "products.online",
            "products.code",
            "products.eancode",
            "products_to_products_information.product_information_id as info_id",
            "title",
            DB::raw('CONCAT(volume, " ", volume_unit) AS volume'),
            "products_information.catalogue",
            (DB::connection("project")->raw("concat(\"<img src='/packages/Dcms/Core/images/flag-\", lcase(selling.country),\".svg' style='width:16px; height: auto;' > \") as country")),
            "selling.id as country_id"
        )
            ->leftJoin('products_to_products_information', 'products.id', '=', 'products_to_products_information.product_id')
            ->leftJoin('products_information', 'products_information.id', '=', 'products_to_products_information.product_information_id')
            ->leftJoin('products_volume_units', 'products.volume_unit_id', '=', 'products_volume_units.id')
            ->leftJoin('languages', 'products_information.language_id', '=', 'languages.id')
            ->leftJoin('products_price', function ($join) {
                $join->on('products_price.product_id', '=', 'products.id');
                $join->on('products_price.country_id', '=', 'languages.country_id');
                $join->on('products_price.price_type', '=', DB::raw("'retail-price'"));
            })
            ->leftJoin('countries as selling', 'products_price.country_id', '=', 'selling.id')
            ->leftJoin('countries as settings', 'languages.country_id', '=', 'settings.id');
            
        if (intval(session('overrule_default_by_language_id')) > 0) {
            $query->where('products_information.language_id', intval(session('overrule_default_by_language_id')));
        }
    
        return DataTables::queryBuilder($query)
            ->addColumn('edit', function ($model) {
                $edit = '<form method="POST" action="/admin/products/' . (isset($model->info_id) ? $model->info_id : $model->id) . '" accept-charset="UTF-8" class="pull-right"> <input name="_token" type="hidden" value="' . csrf_token() . '"> <input name="_method" type="hidden" value="DELETE">
                						<input type="hidden" name="table" value="' . (isset($model->info_id) ? "information" : "product") . '"/>
                						<input type="hidden" name="product_id" value="' . $model->id . '"/>';

                if (Auth::user()->can('products-edit')) {
                    $edit .= '<a class="btn btn-xs btn-default" href="/admin/products/' . $model->id . '/edit"><i class="far fa-pencil"></i></a>';
                }
                if (Auth::user()->can('products-delete')) {
                    $edit .= '<button class="btn btn-xs btn-default" type="submit" value="Delete this product category" onclick="if(!confirm(\'Are you sure to delete this item ? \')){return false;};"><i class="far fa-trash-alt"></i></button>';
                }
                $edit .= '</form>';

                return $edit;
            })
            ->rawColumns(['country', 'edit'])
            ->make(true);
    }

    //return the model/object (id, country, language) or an array e.g. array(language_id => language-COUNTRY)
    public function getCountriesLanguages($returnType = "array")
    {
        //RFC 3066
        $oCountriesLanguages = DB::connection("project")->select('SELECT id, country, language FROM languages ');

        if ($returnType === "model") {
            return $oCountriesLanguages;
        } else {
            $aCountryLanguage = [];

            if (!is_null($oCountriesLanguages) && count($oCountriesLanguages) > 0) {
                foreach ($oCountriesLanguages as $M) {
                    $aCountryLanguage[$M->id] = strtolower($M->language) . "-" . strtoupper($M->country);
                }
            }

            return $aCountryLanguage;
        }
    }

    //return the model/object or an array e.g. array(counrty_id => counryname)
    public function getCountries($returnType = "array")
    {
        $oCountries = DB::connection("project")->select('SELECT id, country_name FROM countries');
        if ($returnType === "model") {
            return $oCountries;
        } else {
            $aCountries = [];

            foreach ($oCountries as $c) {
                $aCountries[$c->id] = $c->country_name;
            }

            return $aCountries;
        }
    }

    public function getTaxClasses($returnType = "array")
    {
        //volumeclasses
        //there is no model for VOLUMES so no eloquent querying here
        $oTaxClasses = DB::connection("project")->select('SELECT id, tax as tax FROM products_price_tax');

        if ($returnType === "model") {
            return $oTaxClasses;
        } else {
            //there was no support for the lists() method
            $aTaxClasses = [];
            foreach ($oTaxClasses as $v) {
                $aTaxClasses[$v->id] = $v->tax;
            }

            return $aTaxClasses;
        }
    }

    public function getVolumesClasses($returnType = "array")
    {
        //volumeclasses
        //there is no model for VOLUMES so no eloquent querying here
        $oVolumeClasses = DB::connection("project")->select('SELECT id, volume_unit as volume FROM products_volume_units ORDER BY 2');

        if ($returnType === "model") {
            return $oVolumeClasses;
        } else {
            //there was no support for the lists() method
            $aVolumesClasses = [];

            foreach ($oVolumeClasses as $v) {
                $aVolumesClasses[$v->id] = $v->volume;
            }

            return $aVolumesClasses;
        }
    }

    //return the model to fill the form
    public function getExtendedModel($id = null)
    {
        if (is_null($id)) {
            return array("Labels"=>DB::connection("project")->select('
														SELECT  products_labels.sort_id,products_labels.id, 0 as checked, concat(\'<img src="\',products_labels_language.image,\'" />\'  )/* group_concat( distinct concat(\'<img src="\',products_labels_language.image,\'" />\'  ) SEPARATOR  \'\' ) */ as image
														FROM products_labels
														INNER JOIN products_labels_language on products_labels_language.label_id = products_labels.id
														GROUP BY (products_labels.id)
														ORDER BY products_labels.sort_id')
                                    ,"aMatter"=>$this->getMatterOptions()
                                    ,"aUsageintent"=>$this->getUsageIntentOptions(true)
                                    ,'pageOptionValuesSelected'=>array()
                                    ,"aUOM"=>$this->getUOMOptions()
                                    ,"aUOMStack"=>$this->getUOMStackOptions()
                                    );
        } else {
            return array("Labels"=>DB::connection("project")->select('
														(SELECT  products_labels.sort_id,products_labels.id, 1 as checked, concat(\'<img src="\',products_labels_language.image,\'" />\'  ) /* group_concat( distinct concat(\'<img src="\',products_labels_language.image,\'" />\'  ) SEPARATOR  \'\' )*/  as image
														FROM products_labels
														INNER JOIN products_labels_language on products_labels_language.label_id = products_labels.id
														INNER JOIN products_to_products_labels ON products_to_products_labels.label_id = products_labels.id
														WHERE products_to_products_labels.product_id = ?
														GROUP BY (products_labels.id))
														UNION
														(SELECT  products_labels.sort_id,products_labels.id, 0 as checked, concat(\'<img src="\',products_labels_language.image,\'" />\'  ) /*group_concat( distinct concat(\'<img src="\',products_labels_language.image,\'" />\'  )  SEPARATOR  \'\')*/ as image
														FROM products_labels
														INNER JOIN products_labels_language on products_labels_language.label_id = products_labels.id
														WHERE label_id NOT IN (SELECT label_id FROM products_to_products_labels WHERE product_id = ?)
														GROUP BY (products_labels.id))
														ORDER BY 1', array($id,$id))
                                    ,"aMatter"=>$this->getMatterOptions()
                                    ,"aUsageintent"=>$this->getUsageIntentOptions(true)
                                    ,'pageOptionValuesSelected'=>$this->setPageOptionValues($this->getSelectedPages($id))
                                    ,"aUOM"=>$this->getUOMOptions()
                                    ,"aUOMStack"=>$this->getUOMStackOptions()
                                );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $languageinformation = $this->getInformation();

        $countries = Country::all();
        $carriers = Carrier::all();

        return View::make('dcmsproducts::products/form')
            ->with('languageinformation', $languageinformation)
            ->with('extendgeneralTemplate', ['template' => $this->extendgeneralTemplate, 'model' => $this->getExtendedModel()])
            ->with('informationtemplate', $this->informationTemplate)//giving null will make a fallback to the default productinformation template on the package
            ->with('product_options', $this->getProductOptions())
            ->with('product_options_selected', [])
            ->with('volumeclasses', $this->getVolumesClasses("array"))
            ->with('taxclasses', $this->getTaxClasses("array"))
            ->with('categoryOptionValues', Category::OptionValueTreeArray(false))
            ->with('sortOptionValues', $this->getSortOptions($languageinformation, 1))
            ->with('countries', $countries)
            ->with('carriers', $carriers)
            ->with('model', $this->getExtendedModel());
    }

    protected function validateProductForm()
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = [
            'code' => 'required',
        ];
        $validator = Validator::make(request()->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::back()//to('admin/products/' . $id . '/edit')
            ->withErrors($validator)
                ->withInput();
        } else {
            return true;
        }
    }

    protected function saveProductPrice(Product $Product)
    {
        $input = request()->all();
        $donotdeleteids = [];

        //---------------------------------------------
        // PRODUCT PRICE (Availability per country)
        //---------------------------------------------
        if (isset($input["price-country-id"]) && count($input["price-country-id"]) > 0) {
            foreach ($input['price-country-id'] as $price_id => $countryid) {
                $pPrice = null;
                $pPrice = Price::find($price_id);  //we make an update when we get an PIM-id(products_data.id) from the form
                if (is_null($pPrice) === true) {  // if we couln't find a Model for the given PIM-id we need to create/add a new one.
                    $pPrice = new Price();
                }
                $pPrice->country_id 	= $input['price-country-id'][$price_id];
                $pPrice->price_type		= str_replace(",", ".", $input['price_type'][$price_id]);
                $pPrice->price = str_replace(",", ".", $input['price'][$price_id]);
                $pPrice->product_id = $Product->id;
                $pPrice->price_valuta_id = $input['price_valuta_id'][$price_id];
                $pPrice->price_tax_id = $input['price_tax_id'][$price_id];
                $pPrice->online = (isset($input['price_online'][$price_id]) && $input['price_online'][$price_id] == 1) ?1:0;

                $pPrice->save();

                $donotdeleteids[$pPrice->id] = $pPrice->id;
            }
        }

        //delete all un-used or recently deleted prices
        $Price = Price::where('product_id', '=', $Product->id);
        if (count($donotdeleteids) > 0) {
            $Price->whereNotIn('id', $donotdeleteids);
        }
        $Price->delete();
    }

    //save the attachments to the model
    protected function saveProductAttachments(Product $Product)
    {
        $input = request()->all();
        $donotdeleteids = [];

        //---------------------------------------------
        // PRODUCT Attachemnts (Availability per language_id)
        //---------------------------------------------
        if (isset($input["attachment-language-id"]) && count($input["attachment-language-id"]) > 0) {
            foreach ($input['attachment-language-id'] as $attachment_id => $language_id) {
                $mAttachment = null;
                $mAttachment = Attachment::find($attachment_id);  //we make an update when we get an PIM-id(products_data.id) from the form
                // if we couln't find a Model for the given PIM-id we need to create/add a new one.
                if (is_null($mAttachment) === true) {
                    $mAttachment = new Attachment();
                }
                $mAttachment->product_id = $Product->id;
                $mAttachment->language_id = $language_id;
                $mAttachment->file = str_replace(",", ".", $input['attachment-file'][$attachment_id]);
                $mAttachment->filename = str_replace(",", ".", $input['attachment-filename'][$attachment_id]);//$Attachment->id;
                $mAttachment->proposed = isset($input['attachment-proposed'][$attachment_id]) ? intval($input['attachment-proposed'][$attachment_id]) : 0;//$Attachment->id;
                $mAttachment->save();
                $donotdeleteids[$mAttachment->id] = $mAttachment->id;
            }
        }

        //delete all un-used or recently deleted prices
        $Attachment = Attachment::where('product_id', '=', $Product->id);
        if (count($donotdeleteids) > 0) {
            $Attachment->whereNotIn('id', $donotdeleteids);
        }
        $Attachment->delete();
    }

    //save the images to the model
    protected function saveProductImages(Product $Product)
    {
        $input = request()->all();
        $donotdeleteids = [];

        //---------------------------------------------
        // PRODUCT Attachemnts (Availability per language_id)
        //---------------------------------------------
        if (isset($input["image-language-id"]) && count($input["image-language-id"]) > 0) {
            foreach ($input['image-language-id'] as $image_id => $language_id) {
                $mImage = null;
                $mImage = Image::find($image_id);  //we make an update when we get an PIM-id(products_data.id) from the form
                // if we couln't find a Model for the given PIM-id we need to create/add a new one.
                if (is_null($mImage) === true) {
                    $mImage = new Image();
                }
                $mImage->product_id = $Product->id;
                $mImage->language_id = $language_id;
                $mImage->file =  $input['image-file'][$image_id];
                $mImage->filename = $input['image-filename'][$image_id];//$image->id;
                $mImage->proposed = isset($input['image-proposed'][$image_id]) ? intval($input['image-proposed'][$image_id]) : 0;//$image->id;
                $mImage->online = isset($input['image-online'][$image_id]) ? intval($input['image-online'][$image_id]) : 0;//$image->id;
                $mImage->save();
                $donotdeleteids[$mImage->id] = $mImage->id;
            }
        }

        //delete all un-used or recently deleted prices
        $Image = Image::where('product_id', '=', $Product->id);
        if (count($donotdeleteids) > 0) {
            $Image->whereNotIn('id', $donotdeleteids);
        }
        $Image->delete();
    }

    /**
     * Store the product based on a productid or a new product
     *
     * @return Product Object
     */
    protected function saveProductProperties($productid = null)
    {
        // do check if the given id is existing.
        if (!is_null($productid) && intval($productid) > 0) {
            $Product = Product::find($productid);
        }

        if (!isset($Product) || is_null($Product)) {
            $Product = new Product();
        }

        foreach ($this->productColumNames as $column => $inputname) {
            if (!request()->has($inputname) && array_key_exists($inputname, $this->productColumnNamesDefaults)) {
                $Product->$column = $this->productColumnNamesDefaults[$inputname];
            } else {
                $Product->$column = request()->get($inputname);
            }
        }

        $Product->algoliasync = 1;
        $Product->save();
        $Product->information()->detach(); //detach any information setting, this will be set up using teh saveProductInformation() method

        //save the options...
        //remove all curent options
        ProductToOptions::where('products_id', $Product->id)->delete();
        //save the newly selected options

        if (request()->has('hazzardP_id')) {
            debug(request()->get('hazzardP_id'));
            foreach (request()->get('hazzardP_id') as $option_id) {
                if (!is_null($option_id)) {
                    $pto = new ProductToOptions();
                    $pto->products_id = $Product->id;
                    $pto->products_options_id = $option_id;
                    $pto->save();
                }
            }
        }

        if (request()->has('hazzardH_id')) {
            foreach (request()->get('hazzardH_id') as $option_id) {
                if (!is_null($option_id)) {
                    $pto = new ProductToOptions();
                    $pto->products_id = $Product->id;
                    $pto->products_options_id = $option_id;
                    $pto->save();
                }
            }
        }

        if (request()->has('nutritionsuitable_id')) {
            foreach (request()->get('nutritionsuitable_id') as $option_id) {
                if (!is_null($option_id)) {
                    $pto = new ProductToOptions();
                    $pto->products_id = $Product->id;
                    $pto->products_options_id = $option_id;
                    $pto->save();
                }
            }
        }

        if (request()->has('singleNutrition_id')) {
            if (!is_null(request()->get('singleNutrition_id'))) {
                $pto = new ProductToOptions();
                $pto->products_id = $Product->id;
                $pto->products_options_id = request()->get('singleNutrition_id');
                $pto->save();
            }
        }

        if (request()->has('straightfertilizationtype_id')) {
            foreach (request()->get('straightfertilizationtype_id') as $option_id) {
                if (!is_null($option_id)) {
                    $pto = new ProductToOptions();
                    $pto->products_id = $Product->id;
                    $pto->products_options_id = $option_id;
                    $pto->save();
                }
            }
        }

        if (request()->has('pestcontrol_id')) {
            foreach (request()->get('pestcontrol_id') as $option_id) {
                if (!is_null($option_id)) {
                    $pto = new ProductToOptions();
                    $pto->products_id = $Product->id;
                    $pto->products_options_id = $option_id;
                    $pto->save();
                }
            }
        }

        if (request()->has('pottingsoiltype_id')) {
            foreach (request()->get('pottingsoiltype_id') as $option_id) {
                if (!is_null($option_id)) {
                    $pto = new ProductToOptions();
                    $pto->products_id = $Product->id;
                    $pto->products_options_id = $option_id;
                    $pto->save();
                }
            }
        }

        if (request()->has('seedtype_id')) {
            if (!is_null(request()->get('seedtype_id'))) {
                $pto = new ProductToOptions();
                $pto->products_id = $Product->id;
                $pto->products_options_id = request()->get('seedtype_id');
                $pto->save();
            }
        }

        if (request()->has('location_id')) {
            if (!is_null(request()->get('location_id'))) {
                $pto = new ProductToOptions();
                $pto->products_id = $Product->id;
                $pto->products_options_id = request()->get('location_id');
                $pto->save();
            }
        }

        if (request()->has('signalword_id')) {
            if (!is_null(request()->get('signalword_id'))) {
                $pto = new ProductToOptions();
                $pto->products_id = $Product->id;
                $pto->products_options_id = request()->get('signalword_id');
                $pto->save();
            }
        }

        if (request()->has('pestcontroltype_id')) {
            if (!is_null(request()->get('pestcontroltype_id'))) {
                $pto = new ProductToOptions();
                $pto->products_id = $Product->id;
                $pto->products_options_id = request()->get('pestcontroltype_id');
                $pto->save();
            }
        }

        if (request()->has('seedperiod_id')) {
            if (!is_null(request()->get('seedperiod_id'))) {
                $pto = new ProductToOptions();
                $pto->products_id = $Product->id;
                $pto->products_options_id = request()->get('seedperiod_id');
                $pto->save();
            }
        }

        if (request()->has('bloomingperiod_id')) {
            if (!is_null(request()->get('bloomingperiod_id'))) {
                $pto = new ProductToOptions();
                $pto->products_id = $Product->id;
                $pto->products_options_id = request()->get('bloomingperiod_id');
                $pto->save();
            }
        }

        if (request()->has('attractivefor_id')) {
            if (!is_null(request()->get('attractivefor_id'))) {
                $pto = new ProductToOptions();
                $pto->products_id = $Product->id;
                $pto->products_options_id = request()->get('attractivefor_id');
                $pto->save();
            }
        }

        if (request()->has('perennialplant_id')) {
            if (!is_null(request()->get('perennialplant_id'))) {
                $pto = new ProductToOptions();
                $pto->products_id = $Product->id;
                $pto->products_options_id = request()->get('perennialplant_id');
                $pto->save();
            }
        }
        return $Product;
    }

    /**
     * Save the ProductInformation to the given product (object)
     * this can be filtered by givin a single languageid - the filter helps for returning the model of the saved Information
     * by default you get back the last saved Information object
     * @return Product Object
     */
    protected function saveProductInformation(Product $Product, $givenlanguage_id = null)
    {
        $input = request()->all();

        $pInformation = null; //pInformation = object product Information

        if (isset($input["information_language_id"]) && count($input["information_language_id"]) > 0) {
            foreach ($input["information_language_id"] as $i => $language_id) {
                if ((is_null($givenlanguage_id) || ($language_id == $givenlanguage_id)) && (strlen(trim($input[$this->informationColumNames['title']][$i])) > 0)) {
                    $pInformation = null; //reset when in a loop
                    $newInformation = true;

                    if (intval($input["information_id"][$i]) > 0) {
                        $pInformation = Information::find($input["information_id"][$i]);
                    }

                    if (!isset($pInformation) || is_null($pInformation)) {
                        $pInformation = new Information();
                    } else {
                        $newInformation = false;
                    }

                    $oldSortID = null;
                    if ($newInformation == false && !is_null($pInformation->sort_id) && intval($pInformation->sort_id) > 0) {
                        $oldSortID = intval($pInformation->sort_id);
                    }


                    foreach ($this->informationColumNames as $column => $inputname) {
                        if ($column <> 'highpriority' && $column <> 'megamenu' && isset($input[$inputname][$i]) && !empty($input[$inputname][$i]) ) {
                            $pInformation->$column = $input[$inputname][$i];
                        } elseif (isset($input[$inputname][$i])&&$column == 'highpriority') {
                            $pInformation->$column = intval($input[$inputname][$i]);
                        } elseif (isset($input[$inputname][$i])&&$column == 'megamenu') {
                            $pInformation->$column = intval($input[$inputname][$i]);
                        } elseif ($column == 'highpriority') {
                            $pInformation->$column = 0;
                        } elseif ($column == 'megamenu') {
                            $pInformation->$column = 0;
                        } else {
                            $pInformation->$column = null;
                        }
                    }

                    if (!isset($input['period_temp_type'][$i]) && !empty($input['period_temp'][$i]) ) {
                        $pInformation->period_temp_type = 'applicable from';
                    } elseif (empty($input['period_temp'][$i])) {
                        $pInformation->period_temp_type = null;
                    }

                    $pInformation->language_id = $input["information_language_id"][$i];//$language_id;
                    $pInformation->product_category_id = ($input[$this->informationColumNames['product_category_id']][$i] == 0 ? null : $input[$this->informationColumNames['product_category_id']][$i]);
                    $pInformation->url_slug = Str::slug($input[$this->informationColumNames['url_slug']][$i]);

                    $pInformation->save();
                    $Product->information()->attach($pInformation->id);
                    
                    $this->setCanonicalUrl($pInformation->id);

                    $sort_incrementstatus = "0"; //the default
                    if (is_null($oldSortID) || $oldSortID == 0) {
                        //update all where sortid >= input::sortid
                        $updateInformations = Information::where('language_id', '=', $language_id)->where('sort_id', '>=', $input[$this->informationColumNames['sort_id']][$i])->where('id', '<>', $pInformation->id)->get(['id', 'sort_id']);
                        $sort_incrementstatus = "+1";
                    } elseif ($oldSortID > $input[$this->informationColumNames['sort_id']][$i]) {
                        $updateInformations = Information::where('language_id', '=', $language_id)->where('sort_id', '>=', $input[$this->informationColumNames['sort_id']][$i])->where('sort_id', '<', $oldSortID)->where('id', '<>', $pInformation->id)->get(['id', 'sort_id']);
                        $sort_incrementstatus = "+1";
                    } elseif ($oldSortID < $input[$this->informationColumNames['sort_id']][$i]) {
                        $updateInformations = Information::where('language_id', '=', $language_id)->where('sort_id', '>', $oldSortID)->where('sort_id', '<=', $input[$this->informationColumNames['sort_id']][$i])->where('id', '<>', $pInformation->id)->get(['id', 'sort_id']);
                        $sort_incrementstatus = "-1";
                    }

                    if ($sort_incrementstatus <> "0") {
                        if (isset($updateInformations) && count($updateInformations) > 0) {
                            foreach ($updateInformations as $uInformation) {
                                if ($sort_incrementstatus == "+1") {
                                    $uInformation->sort_id = intval($uInformation->sort_id) + 1;
                                    $uInformation->save();
                                } elseif ($sort_incrementstatus == "-1") {
                                    $uInformation->sort_id = intval($uInformation->sort_id) - 1;
                                    $uInformation->save();
                                }
                            }//end foreach($updateInformations as $Information)
                        }//end 	if (count($updateInformations)>0)
                    }//$sort_incrementstatus <> "0"
                }//end if($language_id ==$language_id
            }//foreach($input["information_language_id"] as $i => $language_id)
        }//if (isset($input["information_language_id"]) && count($input["information_language_id"])>0)

        // based on the Product->id we can find the attached info's
        // if the attached info's contain a information_group_id we can use this for the rest of the info's
        // otherwise create a new group_id
        $information_group_id = 0; //define var
        $ProductInformationObject = $Product->information; //set it to a var, so the query will run once in this part.

        if ($ProductInformationObject->count() > 0) {
            foreach ($ProductInformationObject as $I) {
                if (intval($I->information_group_id) > 0) {
                    if (!is_null($I->information_group_id) && intval($I->information_group_id) > 0) {
                        $information_group_id = intval($I->information_group_id);
                        break;
                    }
                }
            }
            if ($information_group_id <= 0) {
                $igi = Information::orderBy('information_group_id', 'desc')->take(1)->get();
                $information_group_id = intval($igi[0]->information_group_id) + 1;
            }

            $this->information_group_id = $information_group_id;

            foreach ($ProductInformationObject as $I) {
                $I = Information::find($I->id);
                $I->information_group_id = $information_group_id;
                $I->save();
            }
        }


        return $pInformation;
    }

    public function getInformation($id = null)
    {
        if (is_null($id)) {
            return DB::connection("project")->table("languages")->select((DB::connection("project")->raw("'' as title, '' as description, '' as meta_description, '' as description_short,  NULL as sort_id, (select max(sort_id) from products_information where language_id = languages.id) as maxsort, '' as information_id, '' as id , '' as product_category_id")), "id as language_id", "language", "language_name", "country")->get();
        } else {
            return DB::connection("project")->select('
														SELECT products_information.language_id ,sort_id, (select max(sort_id) from products_information as X  where X.language_id = products_information.language_id) as maxsort, language, language_name, country, products_information.title, products_information.description, products_information.meta_description, products_information.description_short, products_information.id as information_id, product_category_id
														FROM  products
														INNER JOIN products_to_products_information on products.id = products_to_products_information.product_id
														INNER JOIN products_information on products_to_products_information.product_information_id = products_information.id
														INNER JOIN languages on languages.id = products_information.language_id
														WHERE products.id = ?

														UNION
														SELECT languages.id as language_id , 0, (select max(sort_id) from products_information where language_id = languages.id), language, language_name, country, \'\' as title, \'\' as description, \'\' as meta_description, \'\' as description_short, \'\' as information_id , \'\'
														FROM languages
														WHERE id NOT IN (SELECT language_id FROM products_information WHERE id IN (SELECT product_information_id FROM products_to_products_information WHERE product_id = ?)) ORDER BY 1 ', [$id, $id]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $product = Product::with('productDeliveries.country', 'productDeliveries.carrier')->find($id);

        $mPrices = DB::connection("project")->select('SELECT products_price.id , country_id, product_id, country_name, price_type, price,  price_tax_id, online FROM products_price INNER JOIN countries ON countries.id = products_price.country_id WHERE product_id = ? ', [$id]);
        $rowPrices = $this->getPriceRow($mPrices);

        $mAttachments = DB::connection("project")->select('SELECT products_attachments.id , language_id, product_id, file,filename, proposed FROM products_attachments WHERE product_id = ? ORDER BY 2 ', [$id]);
        $rowAttachments = $this->getAttachmentsRow($mAttachments);
        $rowAttachmentsByLang = $this->getAttachmentsRow($mAttachments, false, true);

        $mImages = DB::connection("project")->select('SELECT products_images.id , language_id, product_id, file,filename, proposed, online FROM products_images WHERE product_id = ? ORDER BY 2 ', [$id]);
        $rowImages = $this->getImagesRow($mImages);
        $rowImagesByLang = $this->getImagesRow($mImages, false, true);

        $countries = Country::all();
        $carriers = Carrier::all();

        $languageinformation = $this->getInformation($id);

        return View::make('dcmsproducts::products/form')
            ->with('product', $product)
            ->with('extendgeneralTemplate', ['template' => $this->extendgeneralTemplate, 'model' => $this->getExtendedModel($id)])
            ->with('informationtemplate', $this->informationTemplate)//giving null will make a fallback to the default productinformation template on the package
            ->with('product_options', $this->getProductOptions())
            ->with('product_options_selected', ProductToOptions::where('products_id', $id)->pluck('products_options_id')->toArray())
            ->with('languageinformation', $languageinformation)
            ->with('volumeclasses', $this->getVolumesClasses("array"))
            ->with('rowPrices', $rowPrices)
            ->with('rowAttachments', $rowAttachments)
            ->with('rowAttachmentsByLang', $rowAttachmentsByLang)
            ->with('rowImagesByLang', $rowImagesByLang)
            ->with('categoryOptionValues', Category::OptionValueTreeArray(false))
            ->with('sortOptionValues', $this->getSortOptions($languageinformation))
            ->with('countries', $countries)
            ->with('carriers', $carriers)
            ->with('model', $this->getExtendedModel());
    }

    public function getProductOptions($model = null)
    {
        $options = ProductOptions::all();
        return $options;
    }

    public function getSortOptions($model, $setExtra = 0)
    {
        $SortOptions = [];
        foreach ($model as $M) {
            $increment = 0;
            if ($setExtra > 0) {
                $increment = $setExtra;
            }
            if (intval($M->information_id) <= 0 && !is_null($M->maxsort)) {
                $increment = 1;
            }

            $maxSortID = $M->maxsort;
            if (is_null($maxSortID)) {
                $maxSortID = 1;
            }

            for ($i = 1; $i <= ($maxSortID + $increment); $i++) {
                $SortOptions[$M->language_id][$i] = $i;
            }
        }

        return $SortOptions;
    }


    /**
     * copy the model
     *
     * @param  int $product_id
     * @param  int $country_id //helps limiting the prices copy - we don't need the copied product in all countries..
     *
     * @return Response
     */
    public function copy($product_id, $country_id = 0)
    {
        //COPY THE PRODUCT
        $Newproduct = Product::find($product_id)->replicate();
        $Newproduct->save();

        //COPY THE Information
        $ProductsInformation = Product::with('Information')->where('id', '=', $product_id)->get();

        if (!is_null($ProductsInformation)) {
            foreach ($ProductsInformation as $P) {
                foreach ($P->Information as $pInformation) {
                    $Newproduct->information()->attach($pInformation->id);
                }
            }
        }

        //COPY THE Attachment
        $Attachements = Attachment::where('product_id', '=', $product_id)->get();
        if (!is_null($Attachements)) {
            foreach ($Attachements as $A) {
                $newAttachement = $A->replicate();
                $newAttachement->product_id = $Newproduct->id;
                $newAttachement->save();
            }
        }

        //COPY THE Attachment
        $Images = Image::where('product_id', '=', $product_id)->get();
        if (!is_null($Images)) {
            foreach ($Images as $Image) {
                $newImage = $Image->replicate();
                $newImage->product_id = $Newproduct->id;
                $newImage->save();
            }
        }

        //COPY THE Prices
        $Prices = Price::where('product_id', '=', $product_id)->where('country_id', '=', $country_id)->get();
        if (!is_null($Prices)) {
            foreach ($Prices as $P) {
                $newPrice = $P->replicate();
                $newPrice->product_id = $Newproduct->id;
                $newPrice->save();
            }
        }

        return Redirect::to('admin/products');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->validateProductForm() === true) {
            $Product = $this->saveProductProperties();
            $this->saveProductInformation($Product);
            $this->saveProductPrice($Product);
            $this->saveProductAttachments($Product);
            $this->saveProductImages($Product);

            Session::flash('message', 'Successfully created Product!');
            return Redirect::to('admin/products');
        } else {
            return $this->validateProductForm();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        if ($this->validateProductForm() === true) {
            $Product = $this->saveProductProperties($id);
            $this->saveProductInformation($Product);
            $this->saveProductPrice($Product);
            $this->saveProductAttachments($Product);
            $this->saveProductImages($Product);

            Session::flash('message', 'Successfully updated Product!');
            return Redirect::to('admin/products');
        } else {
            return $this->validateProductForm();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        if (request()->get('table') == 'product') {
            Product::find($id)->delete();
            Price::where("product_id", "=", $id)->delete();
        } else {
            $Information = Information::find($id);
            $Information->products()->detach(request()->get("product_id"));
        }

        Session::flash('message', 'Successfully deleted the Product!');

        return Redirect::to('admin/products');
    }

    public function setCanonicalUrl($products_information_id = 0)
    {
        $doRunResult = DB::SELECT(DB::raw("select concat('UPDATE products_information SET canonical_url = ''', sluggy, ''' where id = ', id, '; ')  as run FROM (
            select products_information.id,  products_information.title,
            
            products_information.language_id, 
            
            case products_information.language_id when 1 then 
            concat('https://dcm-info.be/nl/',  replace(replace(concat(products_categories_language.canonical_url, '/', products_information.url_slug), 'hobby/','hobby/producten/')   , 'pro/', 'pro/producten/'))
            
            when 2 then 
            
            concat('https://dcm-info.be/fr/',replace(replace(concat(products_categories_language.canonical_url, '/', products_information.url_slug), 'hobby/','hobby/produits/')       , 'pro/', 'pro/produits/'))
            
            when 3 then
            
            concat('https://dcm-info.nl/',replace(replace(concat(products_categories_language.canonical_url, '/', products_information.url_slug), 'hobby/','hobby/producten/')      , 'pro/', 'pro/producten/'))
            
            when 6 then
            
            concat('https://dcm-info.fr/',replace(replace(concat(products_categories_language.canonical_url, '/', products_information.url_slug), 'hobby/','hobby/produits/')      , 'pro/', 'pro/produits/'))
            
            when 7 then
            
            concat('https://cuxin-dcm.de/',replace(replace(concat(products_categories_language.canonical_url, '/', products_information.url_slug), 'hobby/','hobby/produkte/')      , 'pro/', 'pro/produkte/'))
            
            when 10 then
            
            concat('https://dcm.green/en/',replace(replace(concat(products_categories_language.canonical_url, '/', products_information.url_slug), 'hobby/','products/')      , 'pro/', 'products/'))
            
            when 11 then
            
            concat('https://dcm-info.com/int/en/',replace(replace(concat(products_categories_language.canonical_url, '/', products_information.url_slug), 'hobby/','products/')      , 'pro/', 'products/'))
            
            end 
            as sluggy
            
            FROM products_information 
            LEFT JOIN products_categories_language ON products_information.product_category_id = products_categories_language.id
            WHERE products_information.id = ".$products_information_id."
            ) as T 
            "));

        foreach ($doRunResult as $r) {
            if (isset($r->run)) {
                DB::SELECT(DB::RAW($r->run));
            }
        }


        $languages = DB::SELECT(DB::RAW('SELECT id, country_id,  regional FROM languages'));
        foreach($languages as $language)
        {
            DB::SELECT(DB::RAW(
                    "CREATE TEMPORARY TABLE tmpproducts_".$language->regional."
                    SELECT `products_information`.id 
                    FROM`products_information` 
                    
 INNER JOIN products_to_products_information  
 ON `products_information` .`id` = products_to_products_information.product_information_id 
 
                    WHERE`products_information` .`language_id`=".$language->id." 
                    AND EXISTS(
                        SELECT*FROM`products` 
                        INNER JOIN`products_to_products_information` ON`products` .`id`=`products_to_products_information` .`product_id` 
                        WHERE`products_information` .`id`=`products_to_products_information` .`product_information_id` 
                        AND`online`=1 AND EXISTS(SELECT*FROM`products_price` WHERE`products` .`id`=`products_price` .`product_id` 
                        AND`country_id`=".$language->country_id." 
                        AND`price_type`='retail-price' AND`online`='1' AND`price` IS NOT NULL))
                    "
            ));
            DB::SELECT(DB::RAW("UPDATE products_information set canonical_url = null where language_id = ".$language->id." AND id NOT IN (SELECT id FROM tmpproducts_".$language->regional.")"));
            DB::SELECT(DB::RAW("DROP TEMPORARY TABLE tmpproducts_".$language->regional));
        }
    }
}
