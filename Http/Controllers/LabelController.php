<?php
namespace Dcms\Products\Http\Controllers;

use Dcms\Core\Http\Controllers\BaseController;
use Dcms\Core\Models\Languages\Language;
use Dcms\Products\Models\Label;
use Dcms\Products\Models\Labeldetail;
use Illuminate\Support\Str;
use View;
use Input;
use Session;
use Validator;
use Redirect;
use DB;
use DataTables;
use Auth;

class LabelController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // load the view and pass the categories
        return View::make('dcmsproducts::labels/index');
    }

    public function getDatatable()
    {
        $query = Labeldetail::query();
        if (intval(session('overrule_default_by_language_id')) > 0) {
            $query->where('language_id', intval(session('overrule_default_by_language_id')));
        }
        $query->get(['id', 'title', 'image', 'language_id', 'label_id']);

        return DataTables::of($query)
            ->addColumn('edit', function ($model) {
                return '<form method="POST" action="/admin/labels/' . $model->id . '" accept-charset="UTF-8" class="pull-right"> <input name="_token" type="hidden" value="' . csrf_token() . '"> <input name="_method" type="hidden" value="DELETE">
								<a class="btn btn-xs btn-default" href="/admin/labels/' . $model->label->id . '/edit"><i class="far fa-pencil"></i></a>
								<button class="btn btn-xs btn-default" type="submit" value="Delete this article" onclick="if(!confirm(\'Are you sure to delete this item?\\r\\nthis may delete more than 1 item\')){return false;};"><i class="far fa-trash-alt"></i></button>
							</form>';
            })
            ->addColumn('image', function ($model) {
                return '<img src="' . $model->image . '"/>';
            })
            ->addColumn('regio', function ($model) {
                return '<img src="/packages/Dcms/Core/images/flag-' . strtolower($model->language->country) . '.svg" style="height:auto; width:16px;" />';
            })
            ->rawColumns(['regio', 'edit', 'image'])
            ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $languages = DB::connection("project")->table("languages")->select((DB::connection("project")->raw("'' as detail_id,'' as title, '' as description, '' as subtitle, '' as slug, '' as image")), "id", "id as language_id", "language", "country", "language_name")->get();

        // load the create form (app/views/categories/create.blade.php)
        return View::make('dcmsproducts::labels/form')
            ->with('languages', $languages);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //	get the category
        $Label = Label::find($id);

        $languages = DB::connection("project")->select('
													SELECT language_id, languages.language, languages.country, languages.language_name, products_labels.id,products_labels.sort_id, products_labels_language.id as detail_id, products_labels_language.label_id, products_labels_language.title,  products_labels_language.subtitle, products_labels_language.slug, products_labels_language.description, products_labels_language.image
													FROM products_labels_language
													LEFT JOIN languages on languages.id = products_labels_language.language_id
													LEFT JOIN products_labels on products_labels.id = products_labels_language.label_id
													WHERE  languages.id is not null AND  label_id = ?
													UNION
													SELECT languages.id , language, country, language_name, \'\' ,\'\' ,\'\' ,\'\' ,\'\' ,\'\' ,\'\' ,\'\' ,\'\'
													FROM languages
													WHERE id NOT IN (SELECT language_id FROM products_labels_language WHERE label_id = ?) ORDER BY 1
													', [$id, $id]);

        return View::make('dcmsproducts::labels/form')
            ->with('Label', $Label)
            ->with('languages', $languages);
    }


    private function validateLabelForm()
    {
        $Languages = Language::all();
        //$rules = array('sort_id'=>'required|integer');
        $rules = [];
        foreach ($Languages as $Lang) {
            //			$rules['title.'.$Lang->id] = 'required';
            //$rules['image.'.$Lang->id] = 'required';
        }

        $validator = Validator::make(request()->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        } else {
            return true;
        }
    }

    private function saveLabelProperties($labelid = null)
    {
        $newLabel = true;
        // do check if the given id is existing.
        if (!is_null($labelid) && intval($labelid) > 0) {
            $Label = Label::find($labelid);
        }

        if (!isset($Label) || is_null($Label)) {
            $Label = new Label;
        } else {
            $newLabel = false;
        }

        $oldSortID = null;
        if ($newLabel == false && !is_null($Label->sort_id) && intval($Label->sort_id) > 0) {
            $oldSortID = intval($Label->sort_id);
        }

        $Label->sort_id = request()->get("sort_id") ?: 0;
        $Label->filter = (request()->has('filter') && intval(request()->get('filter')) == 1)? 1 : 0;
        $Label->save();

        $sort_incrementstatus = "0"; //the default
        if (is_null($oldSortID) || $oldSortID == 0) {
            //update all where sortid >= input::sortid
            $updateLabels = Label::where('sort_id', '>=', $Label->sort_id)->where('id', '!=', $Label->id)->get(['id', 'sort_id']);
            $sort_incrementstatus = "+1";
        } elseif ($oldSortID > request()->get('sort_id')) {
            $updateLabels = Label::where('sort_id', '>=', $Label->sort_id)->where('sort_id', '<', $oldSortID)->where('id', '!=', $Label->id)->get(['id', 'sort_id']);
            $sort_incrementstatus = "+1";
        } elseif ($oldSortID < request()->get('sort_id')) {
            $updateLabels = Label::where('sort_id', '>', $oldSortID)->where('sort_id', '<=', $Label->sort_id)->where('id', '!=', $Label->id)->get(['id', 'sort_id']);
            $sort_incrementstatus = "-1";
        }

        if ($sort_incrementstatus <> "0") {
            if (isset($updateLabels) && count($updateLabels) > 0) {
                //$uInformation for object Information :: update the Information
                foreach ($updateLabels as $uLabel) {
                    if ($sort_incrementstatus == "+1") {
                        $uLabel->sort_id = intval($uLabel->sort_id) + 1;
                        $uLabel->save();
                    } elseif ($sort_incrementstatus == "-1") {
                        $uLabel->sort_id = intval($uLabel->sort_id) - 1;
                        $uLabel->save();
                    }
                }//end foreach($updateInformations as $Information)
            }//end 	if (count($updateLabels)>0)
        }//$sort_incrementstatus <> "0"

        return $Label;
    }

    private function saveLabelDetail(Label $Label, $givenlanguage_id = null)
    {
        $input = request()->all();

        $Labeldetail = null;

        foreach ($input["title"] as $language_id => $title) {
            if ((is_null($givenlanguage_id) || ($language_id == $givenlanguage_id)) && (strlen(trim($input['title'][$language_id])) > 0)) {
                $Labeldetail = null; // reset when in a loop
                $newDetail = true;

                if (intval($input["label_detail_id"][$language_id]) > 0) {
                    $Labeldetail = Labeldetail::find($input["label_detail_id"][$language_id]);
                }
                if (!isset($Labeldetail) || is_null($Labeldetail)) {
                    $Labeldetail = new Labeldetail;
                } else {
                    $newDetail = false;
                }

                $Labeldetail->language_id = $language_id;
                $Labeldetail->title = $input["title"][$language_id];
                $Labeldetail->subtitle = $input["subtitle"][$language_id];
                $Labeldetail->slug = Str::slug($input["title"][$language_id]);
                $Labeldetail->description = $input["description"][$language_id];
                $Labeldetail->image = $input["image"][$language_id];
                //$Labeldetail->admin 			= Auth::dcms()->user()->username;

                $Labeldetail->save();
                Label::find($Label->id)->labeldetail()->save($Labeldetail);
            }
        }

        return $Labeldetail;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->validateLabelForm() === true) {
            $Label = $this->saveLabelProperties();
            $this->saveLabelDetail($Label);

            // redirect
            Session::flash('message', 'Successfully created Label!');

            return Redirect::to('admin/labels');
        } else {
            return $this->validateLabelForm();
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update($id)
    {
        if ($this->validateLabelForm() === true) {
            $Label = $this->saveLabelProperties($id);
            $this->saveLabelDetail($Label);

            // redirect
            Session::flash('message', 'Successfully updated Label!');

            return Redirect::to('admin/labels');
        } else {
            return $this->validateLabelForm();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        //	Labeldetail::where('id', '=', $id)->delete();
        Labeldetail::destroy($id);
        /*

        // delete
        $Labeldetail = Labeldetail::find($id);
        $mainLabelID = $Labeldetail->label_id;

        $Labeldetail->delete();

        if (Labeldetail::where("label_id","=",$mainLabelID)->count() <= 0)
        {
            Label::destroy($mainLabelID);
        }
            */

        // redirect
        Session::flash('message', 'Successfully deleted the Label!');

        return Redirect::to('admin/labels');
    }
}
