<?php
namespace Dcms\Products\Http\Controllers;

use Dcms\Core\Http\Controllers\BaseController;
use Dcms\Products\Models\Category;
use Dcms\Products\Models\Information;
use Dcms\Products\Models\Product;
use Illuminate\Support\Str;
use View;
use Input;
use Session;
use Validator;
use Redirect;
use DB;
use DataTables;
use Auth;

class InformationController extends BaseController
{
    public $informatationColumNames = array();
    public $informatationColumNamesDefaults = array(); // TO DO - the input on the information tab are array based
    public $productColumNames = array();
    public $productColumnNamesDefaults = array(); // e.g. checkboxes left blank will result in NULL database value, if this is not what you want, you can set e.g. array('checkbox_name'=>'0');
    public $extendgeneralTemplate = "";
    public $informationTemplate = "";

    public function __construct()
    {
        $this->informationColumNames = array('title'=>'information_name'
                                             ,'description_short'=>'information_description_short'
                                             ,'description'=>'information_description'
                                             ,'meta_description'=>'information_meta_description'
                                             ,'marketplace_description'=>'marketplace_description'
                                             ,'sort_id'=>'information_sort_id'
                                             ,'product_category_id'=>'product_category_id'
                                             ,'url_slug'=>'information_name'
                                             ,'url_path'=>'information_name'
        );

        // this is custom DCM should be out of the vendor folder
        $this->informationColumNames = array_merge($this->informationColumNames, array( 'new'=>'information_new', 'catalogue'=>'information_catalogue', 'composition'=>'information_composition', 'guarantee'=>'information_guarantee',  'manual'=>'information_manual', 'period'=>'information_period', 'image'=>'information_image', 'doses'=>'information_doses', 'highpriority'=>'highpriority', 'megamenu'=>'megamenu'));
        // end of customisation

        $this->productColumNames = array('code'=>'code'
                                         ,'eancode'=>'eancode'
                                         ,'image'=>'image'
                                         ,'volume'=>'volume'
                                         ,'volume_unit_id'=>'volume_unit_id'
        );

        $this->extendgeneralTemplate = null;
        $this->informationTemplate = null;
    }

    /**
     * get the data for DataTable JS plugin.
     *
     * @return Response
     */
    public function getDatatable()
    {
        $query = DB::connection("project")
        ->table("products_information")
        ->select(
            "products_information.id",
            "information_group_id",
            "title",
            (DB::connection("project")->raw("concat(\"<img src='/packages/Dcms/Core/images/flag-\", lcase(languages.country),\".svg' style='width:16px; height: auto;' > \") as regio"))
        )
        ->leftJoin('languages', 'products_information.language_id', '=', 'languages.id')
        ->orderBy('information_group_id');

        if (intval(session('overrule_default_by_language_id')) > 0) {
            $query->where('products_information.language_id', intval(session('overrule_default_by_language_id')));
        }
    
        return DataTables::queryBuilder($query)
            ->addColumn('edit', function ($model) {
                return '<form method="POST" action="/admin/products/information/'.$model->id.'" accept-charset="UTF-8" class="pull-right"> <input name="_token" type="hidden" value="'.csrf_token().'"> <input name="_method" type="hidden" value="DELETE">
											<a class="btn btn-xs btn-default" href="/admin/products/information/'.(!is_null($model->information_group_id)?$model->information_group_id:"i-".$model->id).'/edit"><i class="far fa-pencil"></i></a>

										</form>';
            })
            ->rawColumns(['regio','edit'])
            ->make(true) ;
    }

    /**
     * get the data for DataTable JS plugin.
     *
     * @return Response
     */
    public function getPlantRelationTable($plant_id = 0)
    {
        $queryA = DB::connection('project')
                                        ->table('products_information as x')
                                        ->select(
                                            (
                                                DB::connection("project")->raw('
                                                    catalogue,
                                                    languages.country,
                                                    information_group_id,
                                                    title,
                                                        case when (select count(*) from products_information_group_to_plants where products_information_group_to_plants.information_group_id = x.information_group_id and plant_id = "'.$plant_id.'") > 0 then 1 else 0 end as checked
                                                    ')
                                            )
                                        )
                                        ->whereNotNull('x.information_group_id')
                                        ->leftJoin('languages', 'x.language_id', '=', 'languages.id')
                                        ->orderBy('checked', 'DESC');

        $queryB = clone $queryA;
        $queryB->whereRaw('case when (select count(*) from products_information_group_to_plants where products_information_group_to_plants.information_group_id = x.information_group_id and plant_id = "'.$plant_id.'") > 0 then 1 else 0 end = 1');

        if (intval(session('overrule_default_by_language_id')) > 0) {
            $queryA->where('x.language_id', intval(session('overrule_default_by_language_id')));
        }

        return Datatables::queryBuilder($queryB->union($queryA)->orderBy('checked', 'desc'))
            ->addColumn('radio', function ($model) {
                return '<input type="checkbox" name="information_group_id[]" value="'.$model->information_group_id.'" '.($model->checked == 1?'checked="checked"':'').' id="chkbox_'.$model->information_group_id.'" > ';
            })
            ->addColumn('language', function ($model) {
                return '<img src="/packages/Dcms/Core/images/flag-'.strtolower($model->country).'.svg" style="width:16px; height: auto;" alt="">';
            })
            ->rawColumns(['radio','language'])
            ->make(true);
    }
    
    /**
     * get the data for DataTable JS plugin.
     *
     * @return Response
     */
    public function getArticleRelationTable($article_id = 0)
    {
        $query = DB::connection('project')
            ->table('products_information as x')
            ->select(
                (
                    DB::connection("project")->raw('
                    case when (select count(*) from articles_to_products_information_group where articles_to_products_information_group.information_group_id = x.information_group_id and article_id = "'.$article_id.'") > 0 then 1 else 0 end as checked,
                    catalogue,
                    title,
                    languages.country,
                    information_group_id')
                )
            )
            ->leftJoin('languages', 'x.language_id', '=', 'languages.id')
            ->whereRaw('x.id IN (SELECT product_information_id FROM products_to_products_information WHERE product_id IN ( SELECT id FROM products WHERE online = 1 ) )')
            ->whereNotNull('x.information_group_id')
            ->orderBy('checked', 'DESC');

        // $queryB = clone $queryA;
        // $queryB->whereRaw('case when (select count(*) from articles_to_products_information_group where articles_to_products_information_group.information_group_id = x.information_group_id and article_id = "'.$article_id.'") > 0 then 1 else 0 end = 1');
            
        // if (intval(session('overrule_default_by_language_id')) > 0) {
        //     $query->where('x.language_id', intval(session('overrule_default_by_language_id')));
        // }

        // return Datatables::queryBuilder($queryB->union($queryA)->orderBy('checked', 'desc'))
        return Datatables::queryBuilder($query)
                    ->addColumn('radio', function ($model) {
                        return '<input type="checkbox" name="information_group_id[]" value="'.$model->information_group_id.'" '.($model->checked == 1?'checked="checked"':'').' id="chkbox_'.$model->information_group_id.'" > ';
                    })
                    ->addColumn('language', function ($model) {
                        return '<img src="/packages/Dcms/Core/images/flag-'.strtolower($model->country).'.svg" style="width:16px; height: auto;" alt="">';
                    })
                    ->rawColumns(['radio','language'])
                    ->make(true);
    }

    
    public function getProductInformationRelationTable($product_id = null)
    {
        $information_group_id = 0;
        $ProductInformation = Product::with('information')->find($product_id);

        if (isset($ProductInformation->information) && !is_null($ProductInformation->information) && $ProductInformation->information()->count()>0) {
            foreach ($ProductInformation->information as $I) {
                if (!is_null($I->information_group_id)) {
                    $information_group_id =  $I->information_group_id;
                    break;
                }
            }
        }
        $query = DB::connection('project')
                    ->table('products_information as x')
                    ->select(
                        (
                            DB::connection("project")->raw('
                                catalogue,
                                languages.country,
                                information_group_id,
                                title,
                                CASE WHEN (	SELECT count(*) 
                                            FROM products_information_to_products_information 
                                            WHERE 
                                            ( 	products_information_to_products_information.information_group_id = x.information_group_id 
                                                AND 
                                                products_information_to_products_information.information_group_id_tag = "'.$information_group_id.'"
                                            )
                                            OR 
                                            ( 	products_information_to_products_information.information_group_id_tag = x.information_group_id 
                                                AND 
                                                information_group_id = "'.$information_group_id.'"
                                            )
                                        ) > 0 
                                        THEN 1 
                                        ELSE 0 END as checked
											')
                        )
                    )
                    ->leftJoin('languages', 'x.language_id', '=', 'languages.id')
                    ->orderBy('checked', 'DESC');
        // $queryB = clone $queryA;
        // $queryB->whereRaw('CASE WHEN (	SELECT count(*)
        //     FROM products_information_to_products_information
        //     WHERE
        //     ( 	products_information_to_products_information.information_group_id = x.information_group_id
        //         AND
        //         products_information_to_products_information.information_group_id_tag = "'.$information_group_id.'"
        //     )
        //     OR
        //     ( 	products_information_to_products_information.information_group_id_tag = x.information_group_id
        //         AND
        //         information_group_id = "'.$information_group_id.'"
        //     )
        // ) > 0
        // THEN 1
        // ELSE 0 END = 1');
        
        // if (intval(session('overrule_default_by_language_id')) > 0) {
        //     $query->where('x.language_id', intval(session('overrule_default_by_language_id')));
        // }
        
        // return Datatables::queryBuilder($queryB->union($queryA)->orderBy('checked', 'desc'))
        return Datatables::queryBuilder($query)
            ->addColumn('radio', function ($model) {
                return '<input type="checkbox" name="information_group_id[]" value="'.$model->information_group_id.'" '.($model->checked == 1?'checked="checked"':'').' id="chkbox_'.$model->information_group_id.'" > ';
            })
            ->addColumn('language', function ($model) {
                return '<img src="/packages/Dcms/Core/images/flag-'.strtolower($model->country).'.svg" style="width:16px; height: auto;" alt="">';
            })
            ->rawColumns(['radio','language'])
            ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return View::make('dcmsproducts::information/index');
    }


    /**
     * return the requested json data.
     *
     * @return json data
     */
    public function json()
    {
        $term = request()->get("term");
        $language_id = intval(request()->get("language"));
        //the json autoload tool needs some
        $pData = Information::select('id', 'title as label', 'description')->where('title', 'LIKE', '%'.$term.'%')->where('language_id', '=', $language_id)->get()->toJson();
        return $pData;
    }


    //return the model/object (id, country, language) or an array e.g. array(language_id => language-COUNTRY)
    public function getCountriesLanguages($returnType = "array")
    {
        //RFC 3066
        $oCountriesLanguages = DB::connection("project")->select('SELECT id, country, language FROM languages ');

        if ($returnType === "model") {
            return $oCountriesLanguages;
        } else {
            $aCountryLanguage = array();
            if (!is_null($oCountriesLanguages) && count($oCountriesLanguages)>0) {
                foreach ($oCountriesLanguages as $M) {
                    $aCountryLanguage[$M->id] = strtolower($M->language)."-".strtoupper($M->country);
                }
            }
            return $aCountryLanguage;
        }
    }


    //return the model/object or an array e.g. array(counrty_id => counryname)
    public function getCountries($returnType = "array")
    {
        $oCountries =  DB::connection("project")->select('SELECT id, country_name FROM countries');
        if ($returnType === "model") {
            return $oCountries;
        } else {
            $aCountries = array();
            foreach ($oCountries as $c) {
                $aCountries[$c->id] = $c->country_name;
            }
            return $aCountries;
        }
    }


    //return the model to fill the form
    public function getExtendedModel()
    {
        //do nothing sit back and make the extension hook up.
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return "disabled - since there must be some volume on an information";
    }

    protected function validateProductForm()
    {
        $rules = array(
            'code'       => 'required',
        );
        $validator = Validator::make(request()->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::back()//to('admin/products/' . $id . '/edit')
            ->withErrors($validator)
                ->withInput();
        } else {
            return true;
        }
    }

    /**
     * Save the ProductInformation to the given product (object)
     * this can be filtered by givin a single languageid - the filter helps for returning the model of the saved Information
     * by default you get back the last saved Information object

     * @return Product Object
     */
    protected function saveProductInformation()
    {
        $input = request()->all();

        $pInformation = null; //pInformation = object product Information
        $aSavedInformationIDs = array();

        if (isset($input["information_language_id"]) && count($input["information_language_id"])>0) {
            foreach ($input["information_language_id"] as $i => $language_id) {
                if ((strlen(trim($input[$this->informationColumNames['title']][$i]))>0)) {
                    $pInformation = null; //reset when in a loop
                    $newInformation = true;
                    if (intval($input["information_id"][$i]) > 0) {
                        $pInformation = Information::find($input["information_id"][$i]);
                    }
                    if (!isset($pInformation) || is_null($pInformation)) {
                        $pInformation = new Information();
                    } else {
                        $newInformation = false;
                    }

                    $oldSortID = null;
                    if ($newInformation == false && !is_null($pInformation->sort_id) && intval($pInformation->sort_id)>0) {
                        $oldSortID = intval($pInformation->sort_id);
                    }

                    foreach ($this->informationColumNames as $column => $inputname) {
                        //				if(isset($input[$inputname][$i])) echo $column . " = ".$input[$inputname][$i]."<br/>";
                        if (isset($input[$inputname][$i]) && $column <> 'new' && $column <> 'megamenu') {
                            $pInformation->$column = $input[$inputname][$i];
                        } elseif (isset($input[$inputname][$i])&&$column == 'new') {
                            $pInformation->$column = intval($input[$inputname][$i]);
                        } elseif (isset($input[$inputname][$i])&&$column == 'highpriority') {
                            $pInformation->$column = intval($input[$inputname][$i]);
                        } elseif (isset($input[$inputname][$i]) && $column == 'megamenu') {
                            $pInformation->$column = intval($input[$inputname][$i]);
                        } elseif ($column == 'new') {
                            $pInformation->$column = 0;
                        } elseif ($column == 'highpriority') {
                            $pInformation->$column = 0;
                        } elseif ($column == 'megamenu') {
                            $pInformation->$column = 0;
                        } else {
                            $pInformation->$column = null;
                        }

                        /*
                            // TO DO
                            //this should go array based input::has('some array name')
                            if( !request()->has($inputname) && array_key_exists($inputname,$this->productColumnNamesDefaults))
                            {
                                $Product->$column = $this->productColumnNamesDefaults[$inputname];
                            }
                            else
                            {
                                $pInformation->$column = $input[$inputname][$i];
                            }
                        */
                    }

                    $pInformation->language_id 	= $input["information_language_id"][$i];//$language_id;
                    $pInformation->product_category_id = ($input[$this->informationColumNames['product_category_id']][$i]==0?null:$input[$this->informationColumNames['product_category_id']][$i]);
                    $pInformation->url_slug 		= Str::slug($input[$this->informationColumNames['url_slug']][$i]);
                    $pInformation->url_path 		= Str::slug($input[$this->informationColumNames['url_path']][$i]);
                    $pInformation->save();
                    
                    $aSavedInformationIDs[] = $pInformation->id;
                    $sort_incrementstatus = "0"; //the default
                    if (is_null($oldSortID) || $oldSortID == 0) {
                        //update all where sortid >= input::sortid
                        $updateInformations = Information::where('language_id', '=', $language_id)->where('sort_id', '>=', $input[$this->informationColumNames['sort_id']][$i])->where('id', '<>', $pInformation->id)->get(array('id','sort_id'));
                        $sort_incrementstatus = "+1";
                    } elseif ($oldSortID > $input[$this->informationColumNames['sort_id']][$i]) {
                        $updateInformations = Information::where('language_id', '=', $language_id)->where('sort_id', '>=', $input[$this->informationColumNames['sort_id']][$i])->where('sort_id', '<', $oldSortID)->where('id', '<>', $pInformation->id)->get(array('id','sort_id'));
                        $sort_incrementstatus = "+1";
                    } elseif ($oldSortID < $input[$this->informationColumNames['sort_id']][$i]) {
                        $updateInformations = Information::where('language_id', '=', $language_id)->where('sort_id', '>', $oldSortID)->where('sort_id', '<=', $input[$this->informationColumNames['sort_id']][$i])->where('id', '<>', $pInformation->id)->get(array('id','sort_id'));
                        $sort_incrementstatus = "-1";
                    }

                    if ($sort_incrementstatus <> "0") {
                        if (isset($updateInformations) && count($updateInformations)>0) {
                            //$uInformation for object Information :: update the Information
                            foreach ($updateInformations as $uInformation) {
                                if ($sort_incrementstatus == "+1") {
                                    $uInformation->sort_id = intval($uInformation->sort_id) + 1;
                                    $uInformation->save();
                                } elseif ($sort_incrementstatus == "-1") {
                                    $uInformation->sort_id = intval($uInformation->sort_id) - 1 ;
                                    $uInformation->save();
                                }
                            }//end foreach($updateInformations as $Information)
                        }//end 	if (count($updateInformations)>0)
                    }//$sort_incrementstatus <> "0"
                }//end if($language_id ==$language_id
            }//foreach($input["information_language_id"] as $i => $language_id)
        }//if (isset($input["information_language_id"]) && count($input["information_language_id"])>0)


        // based on the Product->id we can find the attached info's
        // if the attached info's contain a information_group_id we can use this for the rest of the info's
        // otherwise create a new group_id
        $information_group_id = 0;

        if (count($aSavedInformationIDs)>0) {
            $Information = Information::whereIn('id', $aSavedInformationIDs)->get();

            foreach ($Information as $I) {
                if (!is_null($I->information_group_id)) {
                    $information_group_id = $I->information_group_id;
                    break;
                }
            }
            if ($information_group_id<=0) {
                $igi = Information::orderBy('information_group_id', 'desc')->take(1)->get();
                $information_group_id =	intval($igi[0]->information_group_id)+1;
            }
            foreach ($Information as $I) {
                $I = Information::find($I->id);
                $I->information_group_id = $information_group_id;
                $I->save();
            }
        }

        //	return "saved";
        return $pInformation;
    }

    public function getInformation($information_group_id = null, $information_id = null)
    {
        if (!is_null($information_group_id)) {
            return DB::connection("project")->select('
														SELECT products_information.language_id ,sort_id, (select max(sort_id) from products_information as X  where X.language_id = products_information.language_id) as maxsort, language, language_name, country, products_information.title, products_information.description, products_information.meta_description,products_information.description_short, products_information.marketplace_description  products_information.guarantee, products_information.manual, products_information.period,  products_information.id as information_id, product_category_id, products_information.composition,  products_information.image, products_information.doses, products_information.catalogue, products_information.new
														FROM  products_information
														INNER JOIN languages on languages.id = products_information.language_id
														WHERE information_group_id = ?

														UNION
														SELECT languages.id as language_id , 0, (select max(sort_id) from products_information where language_id = languages.id), language, language_name, country, \'\' as title, \'\' as description, \'\' as meta_description, \'\' as description_short, \'\' as marketplace_description, \'\' as guarantee, \'\' as information_id , \'\' , \'\' , \'\' , \'\' , \'\' , \'\' , \'\' , \'0\'
														FROM languages
														WHERE id NOT IN (SELECT language_id FROM products_information WHERE  information_group_id = ?) ORDER BY 1  ', array($information_group_id,$information_group_id));
        } elseif (!is_null($information_id)) {
            return DB::connection("project")->select('
														SELECT products_information.language_id ,sort_id, (select max(sort_id) from products_information as X  where X.language_id = products_information.language_id) as maxsort, language, language_name, country, products_information.title, products_information.description, products_information.meta_description, products_information.description_short, \'\' as marketplace_description, products_information.guarantee, products_information.manual, products_information.period,  products_information.id as information_id, product_category_id, products_information.composition,  products_information.image, products_information.doses, products_information.catalogue, products_information.new
														FROM  products_information
														INNER JOIN languages on languages.id = products_information.language_id
														WHERE products_information.id = ?

														UNION
														SELECT languages.id as language_id , 0, (select max(sort_id) from products_information where language_id = languages.id), language, language_name, country, \'\' as title, \'\' as description, \'\' as meta_description, \'\' as description_short, \'\' as marketplace_description,  \'\' as guarantee, \'\' as information_id , \'\' , \'\' , \'\' , \'\' , \'\' , \'\' , \'\' , \'0\'
														FROM languages
														WHERE id NOT IN (SELECT language_id FROM products_information WHERE  id = ?) ORDER BY 1  ', array($information_id,$information_id));
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //check if we get the id of the property information_group_id
        if (strstr($id, "i-")===false) {
            $Information = $this->getInformation($id);//  Information::where("information_group_id","=",$id)->get();
        } else {
            $Information = $this->getInformation(null, str_replace("i-", "", $id));
        }

        // show the edit form and pass the product
        return View::make('dcmsproducts::information/form')
            ->with('languageinformation', $Information)
            ->with('sortOptionValues', $this->getSortOptions($Information))
            ->with('categoryOptionValues', Category::OptionValueTreeArray(false));
    }


    public function getSortOptions($model, $setExtra = 0)
    {
        $SortOptions = array();
        foreach ($model as $M) {
            $increment = 0;
            if ($setExtra > 0) {
                $increment = $setExtra;
            }
            if (intval($M->information_id)<=0 && !is_null($M->maxsort)) {
                $increment = 1;
            }

            $maxSortID  = $M->maxsort;
            if (is_null($maxSortID)) {
                $maxSortID = 1;
            }

            for ($i = 1; $i<=($maxSortID+$increment); $i++) {
                $SortOptions[$M->language_id][$i] = $i;
            }
        }
        return $SortOptions;
    }

    /**
     * copy the model
     *
     * @param  int  $product_id
     * @param  int  $country_id //helps limiting the prices copy - we don't need the copied product in all countries..
     * @return Response
     */
    public function copy($product_id, $country_id = 0)
    {
        //	return Redirect::to('admin/products');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        /*
        if ($this->validateProductForm() === true)
        {
            $Product = $this->saveProductProperties();
            $this->saveProductInformation($Product);
            $this->saveProductPrice($Product);
            $this->saveProductAttachments($Product);

            // redirect
            Session::flash('message', 'Successfully created Product!');
            return Redirect::to('admin/products');
        }else return  $this->validateProductForm();
        */
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $this->saveProductInformation();
        Session::flash('message', 'Successfully updated Information!');
        return Redirect::to('admin/products/information');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Session::flash('message', 'Successfully deleted the Product!');
        return Redirect::to('admin/products');
    }
}
