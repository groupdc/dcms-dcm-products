/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table articles_language_to_products_information
# ------------------------------------------------------------

DROP TABLE IF EXISTS `articles_language_to_products_information`;

CREATE TABLE `articles_language_to_products_information` (
  `articles_language_id` int(11) unsigned DEFAULT NULL,
  `products_information_id` int(11) unsigned DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `FK_articlelanguageid` (`articles_language_id`),
  KEY `FK_productinformationid` (`products_information_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table articles_to_products_information_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `articles_to_products_information_group`;

CREATE TABLE `articles_to_products_information_group` (
  `article_id` int(11) unsigned DEFAULT NULL,
  `information_id` int(11) unsigned DEFAULT NULL,
  `information_group_id` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `FK_article` (`article_id`),
  CONSTRAINT `FK_article` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='information_id not relavant, use information_group_id';



# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eancode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `volume` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `volume_unit_id` int(11) unsigned DEFAULT NULL,
  `usage` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usage_intent_id` int(11) unsigned DEFAULT NULL,
  `new` tinyint(11) DEFAULT '0',
  `discontinued` tinyint(4) DEFAULT '0',
  `webshop` tinyint(4) DEFAULT NULL,
  `pickup` tinyint(4) DEFAULT NULL,
  `delivery` tinyint(4) DEFAULT NULL,
  `matter_id` int(11) unsigned DEFAULT NULL,
  `online` tinyint(4) DEFAULT '1',
  `npk` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orderunit_qty` int(11) DEFAULT NULL,
  `uom_id` int(10) unsigned DEFAULT NULL COMMENT 'relatie',
  `uom_stack_id` int(10) unsigned DEFAULT NULL COMMENT 'relatie',
  `admin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_volume_unit_id` (`volume_unit_id`) USING BTREE,
  KEY `FK_prdusage_intent` (`usage_intent_id`),
  KEY `FK_matterid` (`matter_id`),
  CONSTRAINT `FK_matterid` FOREIGN KEY (`matter_id`) REFERENCES `products_matters` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `FK_prdusage_intent` FOREIGN KEY (`usage_intent_id`) REFERENCES `products_usage_intent` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `FK_prdvolume_unit_id` FOREIGN KEY (`volume_unit_id`) REFERENCES `products_volume_units` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`homestead`@`%` */ /*!50003 TRIGGER `before_insert_products` BEFORE INSERT ON `products` FOR EACH ROW BEGIN
    SET new.uuid = uuid();
END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Dump of table products_attachments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_attachments`;

CREATE TABLE `products_attachments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned DEFAULT NULL,
  `language_id` int(11) unsigned DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `admin` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_prdattachmentlanguage` (`language_id`),
  KEY `FK_prdattachproduct` (`product_id`),
  CONSTRAINT `FK_prdattachmentlanguage` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `FK_prdattachproduct` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products_categories_language
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_categories_language`;

CREATE TABLE `products_categories_language` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `language_id` int(11) unsigned DEFAULT NULL,
  `sort_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_short` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_categorylanguage` (`language_id`),
  KEY `parent` (`parent_id`),
  CONSTRAINT `parent` FOREIGN KEY (`parent_id`) REFERENCES `products_categories_language` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `products_categories_language_ibfk_1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table products_information
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_information`;

CREATE TABLE `products_information` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `information_group_id` int(11) unsigned DEFAULT NULL,
  `language_id` int(11) unsigned DEFAULT '1',
  `catalogue` enum('pro','hobby') COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_category_id` int(11) unsigned DEFAULT NULL,
  `sort_id` int(11) unsigned DEFAULT NULL,
  `new` tinyint(4) DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npk` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `composition` varchar(350) COLLATE utf8_unicode_ci DEFAULT NULL,
  `composition_short` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_short` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `guarantee` text COLLATE utf8_unicode_ci,
  `manual` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `doses` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `period` varchar(12) COLLATE utf8_unicode_ci DEFAULT '000000000000',
  `url_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_informationgroupid_language` (`information_group_id`,`language_id`),
  KEY `FK_productlanguage` (`language_id`),
  KEY `FK_productcategory` (`product_category_id`),
  FULLTEXT KEY `ProductSearchHelpertitle` (`title`,`description`,`composition`),
  CONSTRAINT `FK_productcategory` FOREIGN KEY (`product_category_id`) REFERENCES `products_categories_language` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `FK_productlanguage` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table products_information_group_to_conditions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_information_group_to_conditions`;

CREATE TABLE `products_information_group_to_conditions` (
  `conditions_id` int(11) unsigned DEFAULT NULL,
  `information_group_id` int(11) unsigned DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  KEY `FK_prdplant` (`conditions_id`),
  CONSTRAINT `products_information_group_to_conditions_ibfk_1` FOREIGN KEY (`conditions_id`) REFERENCES `conditions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products_information_group_to_media
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_information_group_to_media`;

CREATE TABLE `products_information_group_to_media` (
  `media_id` int(11) DEFAULT NULL,
  `information_id` int(11) unsigned DEFAULT NULL,
  `information_group_id` int(11) unsigned DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products_information_group_to_plants
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_information_group_to_plants`;

CREATE TABLE `products_information_group_to_plants` (
  `plant_id` int(11) unsigned DEFAULT NULL,
  `information_id` int(11) unsigned DEFAULT NULL,
  `information_group_id` int(11) unsigned DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  KEY `FK_prdplant` (`plant_id`),
  CONSTRAINT `FK_prdplant` FOREIGN KEY (`plant_id`) REFERENCES `plants` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products_information_group_to_purchasing_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_information_group_to_purchasing_groups`;

CREATE TABLE `products_information_group_to_purchasing_groups` (
  `purchasing_group_id` int(11) unsigned DEFAULT NULL,
  `information_group_id` int(11) unsigned DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products_labels
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_labels`;

CREATE TABLE `products_labels` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sort_id` int(255) DEFAULT NULL,
  `admin` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products_labels_language
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_labels_language`;

CREATE TABLE `products_labels_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label_id` int(11) unsigned DEFAULT NULL,
  `language_id` int(11) unsigned DEFAULT NULL,
  `sort_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `admin` varchar(50) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_labelid` (`label_id`),
  KEY `FK_lbllanguage_id` (`language_id`),
  CONSTRAINT `FK_labelid` FOREIGN KEY (`label_id`) REFERENCES `products_labels` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `FK_lbllanguage_id` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products_matters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_matters`;

CREATE TABLE `products_matters` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products_matters_language
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_matters_language`;

CREATE TABLE `products_matters_language` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `matter_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned DEFAULT NULL,
  `matter` varchar(100) DEFAULT NULL,
  `online_image` tinyint(4) DEFAULT '0',
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tmatterid` (`matter_id`),
  KEY `FK_matterlanguage` (`language_id`),
  CONSTRAINT `FK_matterlanguage` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `FK_tmatterid` FOREIGN KEY (`matter_id`) REFERENCES `products_matters` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products_price
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_price`;

CREATE TABLE `products_price` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` int(11) unsigned DEFAULT NULL,
  `product_id` int(11) unsigned DEFAULT NULL,
  `price` decimal(6,2) DEFAULT '0.00',
  `price_purchase` decimal(6,2) DEFAULT NULL,
  `price_valuta_id` int(11) unsigned DEFAULT '1',
  `price_tax_id` int(11) unsigned DEFAULT '1',
  `admin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_pricecountry` (`country_id`),
  KEY `FK_priceproduct` (`product_id`),
  CONSTRAINT `FK_pricecountry` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `FK_priceproduct` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table products_price_tax
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_price_tax`;

CREATE TABLE `products_price_tax` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tax` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table products_to_products_information
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_to_products_information`;

CREATE TABLE `products_to_products_information` (
  `product_id` int(11) unsigned NOT NULL,
  `product_information_id` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`product_id`,`product_information_id`),
  KEY `FK_prdtoprdinfo_infoid` (`product_information_id`),
  CONSTRAINT `FK_prdtoprdinfo_infoid` FOREIGN KEY (`product_information_id`) REFERENCES `products_information` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_prdtoprdinfo_prdid` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table products_to_products_labels
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_to_products_labels`;

CREATE TABLE `products_to_products_labels` (
  `product_id` int(11) unsigned NOT NULL,
  `label_id` int(11) unsigned NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`product_id`,`label_id`),
  KEY `FK_prdlabellblid` (`label_id`),
  CONSTRAINT `FK_prdlabellblid` FOREIGN KEY (`label_id`) REFERENCES `products_labels` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_prdlabelprdid` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products_uom
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_uom`;

CREATE TABLE `products_uom` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uom` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products_uom_language
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_uom_language`;

CREATE TABLE `products_uom_language` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uom_id` int(10) unsigned DEFAULT NULL,
  `language_id` int(10) unsigned DEFAULT NULL,
  `uom` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products_uomstack
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_uomstack`;

CREATE TABLE `products_uomstack` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uomstack` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products_uomstack_language
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_uomstack_language`;

CREATE TABLE `products_uomstack_language` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uom_stack_id` int(10) unsigned DEFAULT NULL,
  `language_id` int(10) unsigned DEFAULT NULL,
  `uomstack` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products_usage_intent
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_usage_intent`;

CREATE TABLE `products_usage_intent` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products_usage_intent_language
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_usage_intent_language`;

CREATE TABLE `products_usage_intent_language` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usage_intent_id` int(11) unsigned DEFAULT NULL,
  `language_id` int(11) unsigned DEFAULT NULL,
  `usage_intent` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_usageintent` (`usage_intent_id`),
  KEY `FK_usageintentlangid` (`language_id`),
  CONSTRAINT `FK_usageintent` FOREIGN KEY (`usage_intent_id`) REFERENCES `products_usage_intent` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_usageintentlangid` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table products_volume_units
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_volume_units`;

CREATE TABLE `products_volume_units` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `volume_unit` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table products_volume_units_language
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_volume_units_language`;

CREATE TABLE `products_volume_units_language` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `volume_units_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned DEFAULT NULL,
  `volume_unit` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `volume_unit_long` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `multiple_from_base` int(11) DEFAULT '0' COMMENT 'base,ones,units, "fingers" ; base =1 kilo = 1000',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_volunit` (`volume_units_id`),
  KEY `FK_volunitlang` (`language_id`),
  CONSTRAINT `FK_volunit` FOREIGN KEY (`volume_units_id`) REFERENCES `products_volume_units` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_volunitlang` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table products_volumedata
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products_volumedata`;

CREATE TABLE `products_volumedata` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned DEFAULT NULL,
  `used_for` varchar(255) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


# Dump of table pages_to_products_information_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pages_to_products_information_group`;

CREATE TABLE `pages_to_products_information_group` (
  `page_id` int(11) unsigned DEFAULT NULL,
  `information_id` int(11) unsigned DEFAULT NULL,
  `information_group_id` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='information_id not relavant, use information_group_id';


/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;