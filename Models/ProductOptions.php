<?php

namespace Dcms\Products\Models;

use Dcms\Core\Models\EloquentDefaults;

class ProductOptions extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "products_options";
}
