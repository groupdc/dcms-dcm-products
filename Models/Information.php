<?php

namespace Dcms\Products\Models;

use Dcms\Advices\Models\Condition;
use Dcms\Advices\Models\ProductsInformation;
use Dcms\Core\Models\EloquentDefaults;

class Information extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "products_information";
    protected $fillable = array('language_id', 'title', 'description');

    protected function setPrimaryKey($key)
    {
        $this->primaryKey = $key;
    }

    public function products()
    {
        return $this->belongsToMany('\Dcms\Products\Models\Product', 'products_to_products_information', 'product_information_id', 'product_id')->withTimestamps();
    }

    public function productcategory()
    {
        return $this->belongsTo('\Dcms\Products\Models\Category', 'product_category_id', 'id');
    }

    public function otherlanguages()
    {
        return $this->hasMany('\Dcms\Products\Models\Information', 'information_group_id', 'information_group_id')->whereNotNull('information_group_id');
    }

    public function pages()
    {
        return $this->belongsToMany('\Dcms\Pages\Models\Pageslanguage', 'pages_to_products_information_group', 'information_id', 'page_id')->withTimestamps();
    }

    public function articles()
    {
        return $this->belongsToMany('Dcms\Dcmsarticles\Models\Article', 'articles_to_products_information_group', 'information_id', 'article_id')->withTimestamps();
    }

    public function plants()
    {
        return $this->belongsToMany('Dcms\Plants\Models\Plant', 'products_information_group_to_plants', 'information_id', 'plant_id')->withTimestamps();
    }

    public function conditions()
    {
        return $this->belongsToMany(Condition::class, 'products_information_group_to_conditions', 'information_group_id', 'conditions_id', 'information_group_id', 'id')->distinct();
    }

    public function productinformations()
    {
        return $this->belongsToMany(ProductsInformation::class, 'products_information_to_products_information', 'information_group_id', 'information_group_id_tag', 'information_group_id', 'information_group_id')->distinct()->withTimestamps();
    }
}
