<?php

namespace Dcms\Products\Models;

use App;
use Dcms\Core\Models\EloquentDefaults;

class UOM extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = "products_uom";

    public function uomdetail()
    {
        return $this->belongsTo('Dcms\Products\Models\UOMdetail', 'id', 'order_unit_id');
    }

    public function product()
    {
        //return $this->belongsTo('\Dcweb\Dcms\Models\Products\Product','matter_id');
        return $this->hasMany('Dcms\Products\Models\Product', 'order_unit_id', 'id');
    }
}
