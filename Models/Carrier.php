<?php

namespace Dcms\Products\Models;

use Dcms\Core\Models\EloquentDefaults;

class Carrier extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "carriers";

    public function productDeliveries()
    {
        return $this->hasMany('Dcms\Products\Models\ProductDelivery', 'carrier_id');
    }
}
