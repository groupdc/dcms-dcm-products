<?php

namespace Dcms\Products\Models;

use App;
use Dcms\Core\Models\EloquentDefaults;

class Usageintentdetail extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = "products_usage_intent_language";

    public function usageintent()
    {
        return $this->hasOne('Dcms\Products\Models\Usageintent', 'usage_intent_id', 'id');
    }

    public function language()
    {
        return $this->belongsTo('Dcms\Core\Models\Languages\Language', 'language_id', 'id');
    }
}
