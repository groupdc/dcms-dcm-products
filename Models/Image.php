<?php

namespace Dcms\Products\Models;

use Dcms\Core\Models\EloquentDefaults;

class Image extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "products_images";
    protected $fillable = array('language_id', 'product_id');

    public function product()
    {
        return $this->hasOne('Dcms\Products\Models\Product', 'id', 'product_id');
    }
}
