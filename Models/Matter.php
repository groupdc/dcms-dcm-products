<?php

namespace Dcms\Products\Models;

use App;
use Dcms\Core\Models\EloquentDefaults;

class Matter extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = "products_matters";

    public function matterdetail()
    {
        return $this->belongsTo('Dcms\Products\Models\Matterdetail', 'id', 'matter_id');
    }

    public function product()
    {
        //return $this->belongsTo('\Dcweb\Dcms\Models\Products\Product','matter_id');
        return $this->hasMany('Dcms\Products\Models\Product', 'matter_id', 'id');
    }
}
