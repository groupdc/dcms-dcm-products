<?php

namespace Dcms\Products\Models;

use App;
use Dcms\Core\Models\EloquentDefaults;

class UOMStack extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = "products_uomstack";

    public function uomstackdetail()
    {
        return $this->belongsTo('Dcms\Products\Models\UOMstackdetail', 'id', 'order_stack_id');
    }

    public function product()
    {
        //return $this->belongsTo('\Dcweb\Dcms\Models\Products\Product','matter_id');
        return $this->hasMany('Dcms\Products\Models\Product', 'order_stack_id', 'id');
    }
}
