<?php

namespace Dcms\Products\Models;

use Dcms\Core\Models\EloquentDefaults;

class ProductDelivery extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "products_delivery";

    public function product()
    {
        return $this->belongsTo('Dcms\Products\Models\Product', 'product_id');
    }

    public function country()
    {
        return $this->belongsTo('Dcms\Core\Models\Countries\Country', 'country_id');
    }

    public function carrier()
    {
        return $this->belongsTo('Dcms\Products\Models\Carrier', 'carrier_id');
    }
}
