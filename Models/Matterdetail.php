<?php

namespace Dcms\Products\Models;

use App;
use Dcms\Core\Models\EloquentDefaults;

class Matterdetail extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = "products_matters_language";

    public function matter()
    {
        return $this->hasOne('Dcms\Products\Models\Matter', 'matter_id', 'id');
    }

    public function language()
    {
        return $this->belongsTo('Dcms\Core\Models\Languages\Language', 'language_id', 'id');
    }
}
