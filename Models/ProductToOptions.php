<?php

namespace Dcms\Products\Models;

use Dcms\Core\Models\EloquentDefaults;

class ProductToOptions extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "products_to_options";
}
