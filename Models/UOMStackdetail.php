<?php

namespace Dcms\Products\Models;

use App;
use Dcms\Core\Models\EloquentDefaults;

class UOMStackdetail extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = "products_uomstack_language";

    public function uomstack()
    {
        return $this->hasOne('Dcms\Products\Models\UOMstack', 'order_stack_id', 'id');
    }

    public function language()
    {
        return $this->belongsTo('Dcms\Core\Models\Languages\Language', 'language_id', 'id');
    }
}
