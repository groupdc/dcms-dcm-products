<?php

namespace Dcms\Products\Models;

use App;
use Dcms\Core\Models\EloquentDefaults;

class Usageintent extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = "products_usage_intent";

    public function usageintentdetail()
    {
        return $this->belongsTo('Dcms\Products\Models\Usageintentdetail', 'id', 'usage_intent_id');
    }
}
