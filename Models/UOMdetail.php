<?php

namespace Dcms\Products\Models;

use App;
use Dcms\Core\Models\EloquentDefaults;

class UOMdetail extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = "products_uom_language";

    public function uom()
    {
        return $this->hasOne('Dcms\Products\Models\UOM', 'order_unit_id', 'id');
    }

    public function language()
    {
        return $this->belongsTo('Dcms\Core\Models\Languages\Language', 'language_id', 'id');
    }
}
