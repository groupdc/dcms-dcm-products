<?php

namespace Dcms\Products\Models;

use App;
use Dcms\Core\Models\EloquentDefaults;

class Labeldetail extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = "products_labels_language";

    public function label()
    {
        return $this->belongsTo('Dcms\Products\Models\Label', 'label_id', 'id');
    }

    public function language()
    {
        return $this->belongsTo('Dcms\Core\Models\Languages\Language', 'language_id', 'id');
    }
}
