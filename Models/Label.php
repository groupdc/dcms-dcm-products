<?php

namespace Dcms\Products\Models;

use App;
use Dcms\Core\Models\EloquentDefaults;

class Label extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = "products_labels";

    public function labeldetail()
    {
        return $this->hasMany('Dcms\Products\Models\Labeldetail', 'label_id', 'id');
    }

    public function products()
    {
        /*
        The first argument in belongsToMany() is the name of the class Productdata, the second argument is the name of the pivot table, followed by the name of the product_id column, and at last the name of the product_data_id column.
        */
        return $this->belongsToMany('Dcms\Products\Models\Product', 'products_to_products_labels', 'label_id', 'product_id');
    }
}
