@section('extratabs')
    <li><a href="#labels" role="tab" data-toggle="tab">Labels</a></li>
    <li><a href="#pages" role="tab" data-toggle="tab">Pages</a></li>
    <li><a href="#articles" role="tab" data-toggle="tab">Articles</a></li>
   {{-- <li><a href="#products" role="tab" data-toggle="tab">Products</a></li>--}}
    <li><a href="#plants" role="tab" data-toggle="tab">Plants</a></li>
    <li><a href="#products" role="tab" data-toggle="tab">Products</a></li>
    <li><a href="#conditions" role="tab" data-toggle="tab">Conditions</a></li>
    <li><a href="#webshop-tab" role="tab" data-toggle="tab">Webshop</a></li>
@overwrite

@section('extendedgeneral')
    <!-- #Usage + Usage_intetn (m² - plantent - ...) -->
    <div class="row">
        <div class="col-sm-10">
            <div class="form-group">
                {!! Form::label('usage', 'Intend Usage') !!}
                {!! Form::text('usage', old('usage'), array('class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                {!! Form::label('usage_intent_id', 'Unit') !!}
                {!! Form::select('usage_intent_id', $model["aUsageintent"],null, array('class' => 'form-control')) !!}
            </div>
        </div>
    </div>

    <!-- Matter -->
    <div class="form-group">
        @if(isset($model["aMatter"]) && count($model["aMatter"])>0)
            {!! Form::label('matter_id', 'Matter') !!}
            {!! Form::select('matter_id', $model["aMatter"], null, array('class' => 'form-control')) !!}
        @else
            No Matter found in the DB
        @endif
    </div>

        <!-- NPK -->
        <div class="form-group">
            {!! Form::label('npk', 'N-P-K') !!}
            {!! Form::text('npk', old('npk'), array('class' => 'form-control')) !!}
        </div>

        <!-- NPK -->
        <div class="form-group">
               @if(isset($model["aUOM_sap"]) && count($model["aUOM_sap"])>0)
                    {!! Form::label('uom_id', 'UOM_id !Important this is for webshop articles to make a link to SAP') !!}
                    {!! Form::select('uom_id', $model["aUOM_sap"], null, array('class' => 'form-control')) !!}
                @else
                    No 'unit' found in the DB
                @endif
        </div>

        

    <div class="row">
        <div class="col-sm-4">
            <!-- UOM  unit of measure -->
            <div class="form-group">
                {!! Form::label('order_quantity', 'Order') !!}
                {!! Form::text('order_quantity', old('order_quantity'), array('class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-sm-4">
            <!-- UOM  unit of measure -->
            <div class="form-group">
                @if(isset($model["aUOM"]) && count($model["aUOM"])>0)
                    {!! Form::label('order_unit_id', 'Unit (ref. easy catalog)') !!}
                    {!! Form::select('order_unit_id', $model["aUOM"], null, array('class' => 'form-control')) !!}
                @else
                    No 'unit' found in the DB
                @endif
            </div>
        </div>

        <div class="col-sm-4">
            <!-- UOM  unit of measure -->
            <div class="form-group">
                @if(isset($model["aUOMStack"]) && count($model["aUOMStack"])>0)
                    {!! Form::label('order_stack_id', 'Stack') !!}
                    {!! Form::select('order_stack_id', $model["aUOMStack"], null, array('class' => 'form-control')) !!}
                @else
                    No 'stack' found in the DB
                @endif
            </div>
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('admission_region', 'Admission region') !!}
        {!! Form::text('admission_region', old('admission_region'), array('class' => 'form-control')) !!}
    </div>
    <div class="form-group">
        {!! Form::label('admission_nr', 'Admission nr') !!}
        {!! Form::text('admission_nr', old('admission_nr'), array('class' => 'form-control')) !!}
    </div>

    <div class="form-group">
        {!! Form::label('brick', 'Brick') !!}
        {!! Form::text('brick', old('brick'), array('class' => 'form-control')) !!}
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('width', 'Width (mm)') !!}
                {!! Form::text('width', old('width'), array('class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('height', 'Height (mm)') !!}
                {!! Form::text('height', old('height'), array('class' => 'form-control')) !!}
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::label('depth', 'Depth (mm)') !!}
                {!! Form::text('depth', old('depth'), array('class' => 'form-control')) !!}
            </div>
        </div>
    </div>
@overwrite

@section('extratabcontainter')
    <div id="labels" class="tab-pane">

        <!-- Labels -->
        <div class="form-group">

            {!! Form::label('labels', 'Labels') !!}
            <div class="row">
                <div class="col-sm-12">
                    @if(isset($model["Labels"]) && count($model["Labels"])>0)
                        @foreach($model["Labels"] as $Label)
                            <div class="label-group">
                                {!! Form::checkbox('label['.$Label->id.']', $Label->id, ((old('label['.$Label->id.']')||$Label->checked===1)?true:false), array('class' => 'form-checkbox' , 'id' => 'label-'.$Label->id))!!}
                                {!! HTML::decode(Form::label('label-'.$Label->id, $Label->image, array('class' => ($Label->checked===1)?'checkbox active':'checkbox'))) !!}
                            </div>
                        @endforeach
                    @else
                        No Labels found in the DB
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div id="pages" class="tab-pane">
        <!-- Labels -->
        <div class="form-group">
            <?php
            // *Very simple* recursive rendering function
            function renderNode($node, $pageOptionValuesSelected = array())
            {
                echo '<li class="language-' . $node->language_id . ' depth-' . $node->depth . '  ">';
                echo '<span>';
                if ($node->depth == 0) {
                    echo $node->title . '<i class="far fa-plus-square"></i>';
                } else {
                    $checked = false;
                    if (in_array($node->id, $pageOptionValuesSelected)) {
                        $checked = true;
                    } ?>
            {!! Form::checkbox("page_id[".$node->language_id."][".$node->id."]", $node->id, $checked, array('class' => 'form-checkbox','id'=>'page_id-'.$node->id))  !!}
            {!! Form::label('page_id-'.$node->id, $node->title, array('class' => ($checked == true?'active':'').' checkbox','id'=>'chkbxpage_id-'.$node->id)) !!}
            <?php
                }
                echo '</span>';

                if ($node->children()->count() > 0) {
                    $active = "";
                    if (intval(session('overrule_default_by_language_id')) == $node->language_id) {
                        $active = 'active';
                    }

                    echo "\r\n" . '<ul class="' . ($node->depth == 0 ? 'division '.$active : ($node->depth == 1 ? 'sector' : 'subsector')) . '">' . "\r\n";
                    foreach ($node->children as $child) {
                        renderNode($child, $pageOptionValuesSelected);
                    }
                    echo '</ul>' . "\r\n" . "\r\n";
                }
                echo '</li>' . "\r\n";
            }

            $roots = Dcms\Pages\Models\Pageslanguage::whereIsRoot()->get();

            echo '<ul class="country">';
            foreach ($roots as $root) {
                renderNode($root, $model["pageOptionValuesSelected"]);
            }
            echo '</ul>';
            ?>
        </div>
    </div>

    <div id="articles" class="tab-pane">
        <!-- Labels -->
        <div class="form-group">

            <table id="datatable_article" class="table table-hover table-condensed" style="width:100%">
                <thead>
                <tr>
                    <th></th>
                    <th>Title</th>
                    <th>Country</th>
                    <th>ID</th> 
                  </tr>
              </thead>
            </table>

        </div>
    </div>

    <div id="plants" class="tab-pane">
      <!-- Plants -->
      <div class="form-group">
          <table id="datatable_plants" class="table table-hover table-condensed" style="width:100%">
              <thead>
                  <tr>
                      <th></th>
                      <th>Title</th>
                      <th>Common</th>
                      <th>Country</th>
                      <th>ID</th>
                  </tr>
              </thead>
          </table>
      </div>
    </div>

    <div id="products" class="tab-pane">
        <div class="form-group">
            <table id="datatable-products" class="table table-hover table-condensed" style="width:100%">
                <thead>
                <tr>
                    <th></th>
                    <th>Title</th>
                    <th>Country</th>
                    <th>Division</th>
                    <th>ID</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <div id="conditions" class="tab-pane">
        <!-- Labels -->
        <div class="form-group">

            <table id="datatableconditions" class="table table-hover table-condensed" style="width:100%">
                <thead>
                <tr>
                    <th></th>
                    <th>Title</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <div id="webshop-tab" class="tab-pane">


        <table class="table table-bordered table-striped" id="product-delivery-table">
            <thead>
            <tr>
                <th>Country</th>
                <th>Carrier</th>
                <th>Occupation</th>
                <th>Options</th>
                <th></th>
            </tr>
            </thead>

            <tbody>
            @if(isset($product))
                @foreach($product->productDeliveries as $productDelivery)
                    @include('dcmsproducts::products.partials.product_delivery_row', ['productDelivery' => $productDelivery])
                @endforeach
            @endif
            </tbody>

            <tfoot>
            <tr>
                <td colspan="5">
                    <button class="btn btn-default pull-right add-product-delivery-row" type="button"><i class="far fa-plus"></i></button>
                </td>
            </tr>
            </tfoot>
        </table>

        <div id="removed-product-deliveries"></div>
    </div>

@overwrite

<script language="javascript" type="application/javascript">
    conditionsTable = null;
    plantsTable = null;
    productTable = null;
    articlesTable = null;

    function resetTables(){
        conditionsTable.search('').draw();
        plantsTable.search('').draw();
        productTable.search('').draw();
        articlesTable.search('').draw();
        return true;
    }
    
    $(document).ready(function () {
        $(".checkbox-group label").click(function () {
            var period = "";
            $(this).closest(".period-group").find(".checkbox-group").each(function (i) {
                if ($(this).find("label").hasClass("active")) {
                    period = period.concat("1");
                } else {
                    period = period.concat("0");
                }

                $(this).closest(".period-group").find(".form-control").val(period);
            });
        });

        //pagetree
        $(".country span").click(function () {
            $(this).find('i').toggleClass('fa-minus-square');
            $(this).next().toggleClass('active');
        });

        
        <?php $product_id_helper = isset($product)?$product->id:0; ?>
        
        articlesTable = $('#datatable_article').DataTable({
                      "paging": false,
                      "processing": true,
                      "serverSide": false,
                      "ajax": "{{ route('admin.articles.api.relation.table',array('product_id'=>$product_id_helper)) }}",
                      "columns": [
                          {data: 'radio', name: 'radio'},
                          {data: 'title', name: 'title'},
                          {data: 'language', name: 'language'},
                          {data: 'article_id', name: 'article_id'},
                      ],
                      "createdRow": function( row, data, cells ) {
                            if (~data['radio'].indexOf('checked')) { 
                            $(row).addClass('selected');
                            }
                        }
                  });

        $('#datatable_article').on('click', 'tbody tr', function () {
            if($(e.target).is('input[type=checkbox]')) {
            $(this).toggleClass('selected');
            } else {
                var clicked = $(this).find('input:checkbox');
                if ( clicked.is(':checked')) {
                    clicked.prop('checked', false);
                } else {
                    clicked.prop('checked', true);
                }
    
                $(this).toggleClass('selected');
            }
        });
                  
        
        plantsTable = $('#datatable_plants').DataTable({
            "paging": false,
            "processing": true,
            "ajax": "{{ route('admin.products.api.table.plantrelation',array('product_id'=>$product_id_helper)) }}",
            "columns": [
                {data: 'radio', name: 'radio', orderable: false, searchable: false},
                {data: 'title', name: 'title'},
                {data: 'common', name: 'common'},
                {data: 'language', name: 'language'},
                {data: 'id', name: 'id'},
            ]
        });
              
        productTable = $('#datatable-products').DataTable({
            "processing": true,
            "paging": false,
            "ajax": "{{ route('admin.products.api.table.productinformationrelation',$product_id_helper) }}",
            "columns": [
                {data: 'radio', name: 'radio'},
                {data: 'title', name: 'title'},
                {data: 'language', name: 'language'},
                {data: 'catalogue', name: 'catalogue'},
                {data: 'information_group_id', name: 'information_group_id'},
            ],
            "createdRow": function( row, data, cells ) {
                if (~data['radio'].indexOf('checked')) {
                    $(row).addClass('selected');
                }
            }
        });

        conditionsTable = $('#datatableconditions').DataTable({
                        "paging": false,
                        "processing": true,
                        "serverSide": false,
                        "ajax": "{{ route('admin.conditions.api.productrelationtable',$product_id_helper) }}",
                        "columns": [
                            {data: 'radio', name: 'radio', orderable: false, searchable: false},
                            {data: 'condition', name: 'condition'}
                        ]
                    });

        $(".add-product-delivery-row").click(function () {
                $.ajax({
                    method: 'POST',
                    url: "{{ route('admin.products.api.productdeliveryrow') }}",
                    data: {_token: '{{ csrf_token() }}'},
                    success: function (result) {
                        $("#product-delivery-table tbody").append(result);
                    }
                });
            });

        $(document).on('click', '.remove-product-delivery-row', function () {
            var id = $(this).data('id');
            $(this).closest('tr').remove();
            $('#removed-product-deliveries').append('<input type="hidden" name="removed_product_deliveries[]" value="' + id + '">');
        });

    });

</script>

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">

<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>
