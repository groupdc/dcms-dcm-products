@section('information')

    @foreach($languageinformation as $key => $information)

        <div id="{!! $information->language . '-' . $information->country !!}" class="tab-pane {!! ((intval(session('overrule_default_by_language_id')) == intval($information->language_id)) ? 'active' : ((intval(session('overrule_default_by_language_id')) == 0 && $key == 0) ? 'active': '')) !!}">

            {!! Form::hidden('information_language_id[' . $key . ']', $information->language_id) !!}

            <div class="row">
                <div class="col-sm-10">
                    <div class="form-group">
                        {!! Form::label('information_name[' . $key . ']', 'Product Name') !!}
                        {!! Form::text('information_name[' . $key . ']', (old('information_name[' . $key . ']') ? old('information_name[' . $key . ']') : $information->title ), array('class' => 'form-control')) !!}
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="form-group">
                        {!! Form::label('information_id[' . $key . ']', 'ID') !!}
                        <div class="input-group">
                            {!! Form::text('information_id[' . $key . ']', (old('information_id[' . $key . ']') ? old('information_id[' . $key . ']') : $information->information_id ), array('class' => 'form-control', 'readonly')) !!}
                            <span class="input-group-btn">{!! Form::button('Reset', array('class' => 'btn btn-primary information-id-reset')) !!}</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('information_catalogue[' . $key . ']', 'Catalogue') !!}
                {!! Form::select('information_catalogue[' . $key . ']', array('hobby'=>'Hobby','pro'=>'Pro'), (old('information_catalogue[' . $key . ']') ? old('information_catalogue[' . $key . ']') : $information->catalogue), array('class' => 'form-control')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('information_category_id[' . $key . ']', 'Category') !!}
                {!! isset($categoryOptionValues[$information->language_id])? Form::select('information_category_id[' . $key . ']', $categoryOptionValues[$information->language_id], (old('information_category_id[' . $key . ']') ? old('information_category_id[' . $key . ']') : $information->product_category_id), array('class' => 'form-control')):'' !!}
            </div>

            <div class="form-group">
                {!! Form::checkbox('information_new[' . $key . ']',1, (old('information_new[' . $key . ']') ? old('information_new[' . $key . ']') : $information->new), array('class' => 'form-checkbox' , 'id' => 'information_new[' . $key . ']'))!!}
                {!! HTML::decode(Form::label('information_new[' . $key . ']', "New Product", array('class' =>  ($information->new==1)?'checkbox active new':'checkbox new'))) !!}
            </div>

<div class="form-group">
    {!! Form::checkbox('highpriority[' . $key . ']',1, (old('highpriority[' . $key . ']') ? old('highpriority[' . $key . ']') : $information->highpriority), array('class' => 'form-checkbox' , 'id' => 'highpriority[' . $key . ']'))!!}
    {!! HTML::decode(Form::label('highpriority[' . $key . ']', "High Priority", array('class' =>  ($information->highpriority==1)?'checkbox active highpriority':'checkbox highpriority'))) !!}
</div>

<div class="form-group">
    {!! Form::checkbox('megamenu[' . $key . ']',1, (old('megamenu[' . $key . ']') ? old('megamenu[' . $key . ']') : $information->megamenu), array('class' => 'form-checkbox' , 'id' => 'megamenu[' . $key . ']'))!!}
    {!! HTML::decode(Form::label('megamenu[' . $key . ']', "Mega menu", array('class' =>  ($information->megamenu==1)?'checkbox active megamenu':'checkbox megamenu'))) !!}
</div>
	
            <div class="form-group">
                {!! Form::label('information_sort_id[' . $key . ']', 'Sort') !!}
                {!! Form::select('information_sort_id[' . $key . ']', $sortOptionValues[$information->language_id], (old('information_sort_id[' . $key . ']') ? old('information_sort_id[' . $key . ']') : $information->sort_id), array('class' => 'form-control')) !!}
            </div>
	    
            <div class="form-group">
                {!! Form::label('information_composition[' . $key . ']', 'Composition') !!}
                {!! Form::text('information_composition[' . $key . ']', (old('information_composition[' . $key . ']') ? old('information_composition[' . $key . ']') : $information->composition ), array('class' => 'form-control')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('information_composition_short[' . $key . ']', 'Composition short') !!}
                {!! Form::text('information_composition_short[' . $key . ']', (old('information_composition_short[' . $key . ']') ? old('information_composition_short[' . $key . ']') : $information->composition_short ), array('class' => 'form-control')) !!}
            </div>
	
            <div class="form-group">
                {!! Form::label('image', 'Image') !!}
                <div class="input-group">
                    {!! Form::text('information_image['.$key.']', (old('information_image[' . $key . ']') ? old('information_image[' . $key . ']') : $information->image) , array('class' => 'form-control','id'=>'information_image'.$key)) !!}
                    <span class="input-group-btn">{!! Form::button('Browse Server', array('class' => 'btn btn-primary browse-server', 'id'=>'browse_information_image'.$key)) !!}</span>
                </div>
            </div>
            
            <div class="form-group">
                {!! Form::label('information_description_short[' . $key . ']', 'Short Description') !!}
                {!! Form::textarea('information_description_short[' . $key . ']', (old('information_description_short[' . $key . ']') ? old('information_description_short[' . $key . ']') : $information->description_short ), array('class' => 'form-control')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('information_description[' . $key . ']', 'Long Description') !!}
                {!! Form::textarea('information_description[' . $key . ']', (old('information_description[' . $key . ']') ? old('information_description[' . $key . ']') : $information->description ), array('class' => 'form-control ckeditor')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('meta_description[' . $information->language_id . ']', 'Meta Description') !!}
                {!! Form::text('meta_description[' . $information->language_id . ']', (old('meta_description[' . $key . ']') ? old('meta_description[' . $key . ']') : $information->meta_description ), array('class' => 'form-control')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('information_guarantee[' . $key . ']', 'Guarantee') !!}
                {!! Form::textarea('information_guarantee[' . $key . ']', (old('information_guarantee[' . $key . ']') ? old('information_guarantee[' . $key . ']') : $information->guarantee), array('class' => 'form-control ckeditor')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('information_manual[' . $key . ']', 'Manual') !!}
                {!! Form::textarea('information_manual[' . $key . ']', (old('information_manual[' . $key . ']') ? old('information_manual[' . $key . ']') : $information->manual), array('class' => 'form-control ckeditor')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('information_out_of_stock[' . $key . ']', 'Out of stock') !!}
                {!! Form::textarea('information_out_of_stock[' . $key . ']', (old('information_out_of_stock[' . $key . ']') ? old('information_out_of_stock[' . $key . ']') : $information->out_of_stock), array('class' => 'form-control ckeditor')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('information_how_to_use[' . $key . ']', 'How to use') !!}
                {!! Form::textarea('information_how_to_use[' . $key . ']', (old('information_how_to_use[' . $key . ']') ? old('information_how_to_use[' . $key . ']') : $information->how_to_use), array('class' => 'form-control ckeditor')) !!}
            </div>

<div class="form-group">
    {!! Form::label('information_usp[' . $key . ']', 'USP') !!}
    {!! Form::textarea('information_usp[' . $key . ']', (old('information_usp[' . $key . ']') ? old('information_usp[' . $key . ']') : $information->usp), array('class' => 'form-control ckeditor')) !!}
</div>

<div class="form-group">
                {!! Form::label('video_url[' . $key . ']', 'Video URL') !!}
                {!! Form::text('video_url[' . $key . ']', (old('video_url[' . $key . ']') ? old('video_url[' . $key . ']') : $information->video_url), array('class' => 'form-control')) !!}
            </div>

<div class="form-group">
    {!! Form::label('content[' . $key . ']', 'Content') !!}
    {!! Form::textarea('content[' . $key . ']', (old('content[' . $key . ']') ? old('content[' . $key . ']') : $information->content), array('class' => 'form-control ckeditor')) !!}
</div>
            
            <div class="form-group">
                {!! Form::label('marketplace_description[' . $key . ']', 'Marketplace Description') !!}
                {!! Form::textarea('marketplace_description[' . $key . ']', (old('marketplace_description[' . $key . ']') ? old('marketplace_description[' . $key . ']') : $information->marketplace_description ), array('class' => 'form-control ckeditor')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('information_doses[' . $key . ']', 'Dose') !!}
                {!! Form::text('information_doses[' . $key . ']', (old('information_doses[' . $key . ']') ? old('information_doses[' . $key . ']') : $information->doses), array('class' => 'form-control')) !!}
            </div>

            <div class="form-group clearfix period-group hidden d-none">
                {!! Form::label('information_period[' . $key . ']', 'Period', array('class' => 'full')) !!}
                <div class="clearfix"></div>
                {!! Form::hidden('information_period[' . $key . ']', (old('information_period[' . $key . ']') ? old('information_period[' . $key . ']') : $information->period), array('class' => 'form-control')) !!}

                @for($monthid=1; $monthid<=12 ; $monthid++)
                    <div class="checkbox-group">
                        {!! Form::checkbox('periodehelper['.$monthid.']', 1, ((isset($information->period) && isset($information->period) && substr($information->period,($monthid-1),1) == 1)?true:false), array('class' => 'form-checkbox periodehelper' ,  'id' => 'periode-'.$monthid)) !!}
                        {!! Form::label('periode-'.$monthid, date('M',mktime(0,0,0,$monthid)), array('class' => (isset($information->period) && isset($information->period) && substr($information->period,($monthid-1),1) == 1)?'checkbox active':'checkbox')) !!}
                    </div>
                @endfor
            </div>

            <div class="form-group clearfix period-box" id="period[{!! $key !!}]">
                {!! Form::label('information_period[' . $key . ']', 'Period', array('class' => 'full')) !!}
                {!! Form::hidden('information_period[' . $key . ']', (old('information_period[' . $key . ']') ? old('information_period[' . $key . ']') : $information->period), array('class' => 'form-control')) !!}
                <div class="clearfix"></div>
                <div class="checkbox-group">
                    {!! Form::label('jan', 'Jan') !!}
                    <input type="hidden" name="jan01" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                    <input type="hidden" name="jan02" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                </div>
                <div class="checkbox-group">
                    {!! Form::label('feb', 'Feb') !!}
                    <input type="hidden" name="feb01" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                    <input type="hidden" name="feb02" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                </div>
                <div class="checkbox-group">
                    {!! Form::label('mar', 'Mar') !!}
                    <input type="hidden" name="mar01" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                    <input type="hidden" name="mar02" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                </div>
                <div class="checkbox-group">
                    {!! Form::label('apr', 'Apr') !!}
                    <input type="hidden" name="apr01" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                    <input type="hidden" name="apr02" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                </div>
                <div class="checkbox-group">
                    {!! Form::label('may', 'May') !!}
                    <input type="hidden" name="may01" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                    <input type="hidden" name="may02" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                </div>
                <div class="checkbox-group">
                    {!! Form::label('jun', 'Jun') !!}
                    <input type="hidden" name="jun01" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                    <input type="hidden" name="jun02" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                </div>
                <div class="checkbox-group">
                    {!! Form::label('jul', 'Jul') !!}
                    <input type="hidden" name="jul01" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                    <input type="hidden" name="jul02" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                </div>
                <div class="checkbox-group">
                    {!! Form::label('aug', 'Aug') !!}
                    <input type="hidden" name="aug01" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                    <input type="hidden" name="aug02" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                </div>
                <div class="checkbox-group">
                    {!! Form::label('sep', 'Sep') !!}
                    <input type="hidden" name="sep01" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                    <input type="hidden" name="sep02" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                </div>
                <div class="checkbox-group">
                    {!! Form::label('okt', 'Oct') !!}
                    <input type="hidden" name="oct01" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                    <input type="hidden" name="oct02" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                </div>
                <div class="checkbox-group">
                    {!! Form::label('nov', 'Nov') !!}
                    <input type="hidden" name="nov01" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                    <input type="hidden" name="nov02" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                </div>
                <div class="checkbox-group">
                    {!! Form::label('dec', 'Dec') !!}
                    <input type="hidden" name="dec01" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                    <input type="hidden" name="dec02" data-palette='[{"0": "#dadada"},{"1": "#81bb26"},{"2": "#c7dd9b"}]' value="0">
                </div>
                <script>
                    $period = $("input[id='information_period[{!! $key !!}]']").val();
                    $months = ["jan01", "jan02", "feb01", "feb02", "mar01", "mar02", "apr01", "apr02", "may01", "may02", "jun01", "jun02", "jul01", "jul02", "aug01", "aug02", "sep01", "sep02", "oct01", "oct02", "nov01", "nov02", "dec01", "dec02"];

                    for (var i = 0; i < $period.length; i++) {
                        $("div[id='period[{!! $key !!}]'] .checkbox-group input[name='" + $months[i] + "']").val($period.charAt(i));
                    }
                </script>
            </div>

            <div class="form-group">
                    <label>Application Type </label>  <br/>
                    <input type="radio" name="period_type[{{$key}}]" @if($information->period_type == "applicable") checked @endif value="applicable" id="period_type_applicable_{{$key}}"/> <label style="font-weight:normal;" for="period_type_applicable_{{$key}}">Applicable</label>
                    <input type="radio" name="period_type[{{$key}}]" @if($information->period_type == "first/second application") checked @endif value="first/second application" id="period_type_firstseccond_{{$key}}"/> <label style="font-weight:normal;" for="period_type_firstseccond_{{$key}}">First/second application</label>
                    <input type="radio" name="period_type[{{$key}}]" @if($information->period_type == "ideal application") checked @endif value="ideal application" id="period_type_ideal_{{$key}}"/> <label style="font-weight:normal;" for="period_type_ideal_{{$key}}" >Ideal/regular application </label>
                    <input type="radio" name="period_type[{{$key}}]" @if($information->period_type == null) checked @endif value="" id="period_type_na_{{$key}}"/> <label style="font-weight:normal;" for="period_type_na_{{$key}}"> - </label>
            </div>

            <div class="form-group">
                    <label>Temperature</label>
                    <select name="period_temp[{{$key}}]">
                        <?php 
                         echo "<option value=''> no °C constraint </option>";
                        for($tempcounter = -10; $tempcounter <= 20 ; $tempcounter++  ){     
                            $selected = ""; 
                            if ($information->period_temp != null &&  $information->period_temp == $tempcounter) {
                                $selected = 'selected';
                            }
                            echo "<option value='".$tempcounter."' ".$selected.">".$tempcounter."° C</option>";
                        }
                        ?>
                    </select>

                    <input type="checkbox" name="period_temp_type[{{$key}}]" @if($information->period_temp_type == "soil temperature") checked @endif value="soil temperature"/> Soil temperature
            </div>


            <div class="form-group"><hr/>
                    <u>Data for technical datasheet</u>
            </div>
            

            <div class="form-group">
                {!! Form::label('long_composition[' . $key . ']', 'Long composition') !!}
                {!! Form::textarea('long_composition[' . $key . ']', (old('long_composition[' . $key . ']') ? old('long_composition[' . $key . ']') : $information->long_composition ), array('class' => 'form-control ckeditor')) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('features[' . $key . ']', 'Features') !!}
                {!! Form::textarea('features[' . $key . ']', (old('features[' . $key . ']') ? old('features[' . $key . ']') : $information->features ), array('class' => 'form-control ckeditor')) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('tips_tds[' . $key . ']', 'Tips') !!}
                {!! Form::textarea('tips_tds[' . $key . ']', (old('tips_tds[' . $key . ']') ? old('tips_tds[' . $key . ']') : $information->tips_tds ), array('class' => 'form-control ckeditor')) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('form_tds[' . $key . ']', 'Matter') !!}
                {!! Form::textarea('form_tds[' . $key . ']', (old('form_tds[' . $key . ']') ? old('form_tds[' . $key . ']') : $information->form_tds ), array('class' => 'form-control ckeditor')) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('stock_tds[' . $key . ']', 'Stockage') !!}
                {!! Form::textarea('stock_tds[' . $key . ']', (old('stock_tds[' . $key . ']') ? old('stock_tds[' . $key . ']') : $information->stock_tds ), array('class' => 'form-control ckeditor')) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('safety_tds[' . $key . ']', 'Safety') !!}
                {!! Form::textarea('safety_tds[' . $key . ']', (old('safety_tds[' . $key . ']') ? old('safety_tds[' . $key . ']') : $information->safety_tds ), array('class' => 'form-control ckeditor')) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('dose_tds[' . $key . ']', 'Dose') !!}
                {!! Form::textarea('dose_tds[' . $key . ']', (old('dose_tds[' . $key . ']') ? old('dose_tds[' . $key . ']') : $information->dose_tds ), array('class' => 'form-control ckeditor')) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('identification_tds[' . $key . ']', 'Identification') !!}
                {!! Form::textarea('identification_tds[' . $key . ']', (old('identification_tds[' . $key . ']') ? old('identification_tds[' . $key . ']') : $information->identification_tds ), array('class' => 'form-control ckeditor')) !!}
            </div>


        </div>
    @endforeach
@overwrite
