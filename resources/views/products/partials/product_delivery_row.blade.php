@php
    $id = $productDelivery->exists ? $productDelivery->id : uniqid('new_');
    $check_webshop = isset($productDelivery) && $productDelivery->webshop;
    $check_pickup = isset($productDelivery) && $productDelivery->pickup;
    $check_delivery = isset($productDelivery) && $productDelivery->delivery;
    $check_message = isset($productDelivery) && $productDelivery->message;
@endphp

<tr>
    <td>
        <select name="product_deliveries[{{$id}}][country_id]" id="product_deliveries[{{$id}}][country_id]" class="form-control">
            @foreach($countries as $country)
                <option value="{{$country->id}}" {{$productDelivery->country_id == $country->id ? 'selected' : ''}}>{{ $country->country_name }}</option>
            @endforeach
        </select>
    </td>
    <td>
        <select name="product_deliveries[{{$id}}][carrier_id]" id="product_deliveries[{{$id}}][country_id]" class="form-control">
            @foreach($carriers as $carrier)
                <option value="{{$carrier->id}}" {{$productDelivery->carrier_id == $carrier->id ? 'selected' : ''}}>{{ $carrier->name }}</option>
            @endforeach
        </select>
    </td>
    <td>
        <select name="product_deliveries[{{$id}}][occupation]" id="product_deliveries[{{$id}}][occupation]" class="form-control">
            @for($i = 0 ; $i<= 9 ; $i++)
                <option value="{{floatval($i/10)}}" {{ floatval($i/10) == floatval($productDelivery->occupation) ? 'selected' : ''}}>{{floatval($i/10)}}</option>
            @endfor
            <option value="1" {{floatval(1) == floatval($productDelivery->occupation) ? 'selected' : ''}}>1</option>
        </select>
    </td>
    <td>
        {!! Form::checkbox('product_deliveries['.$id.'][webshop]', 1, $check_webshop, array('class' => 'form-checkbox','id'=>'product_deliveries['.$id.'][webshop]'))  !!}
        {!! HTML::decode(Form::label('product_deliveries['.$id.'][webshop]', 'Webshop', array('class' => $check_webshop?'checkbox active':'checkbox'))) !!}

        {!! Form::checkbox('product_deliveries['.$id.'][pickup]', 1, $check_pickup, array('class' => 'form-checkbox' , 'id' => 'product_deliveries['.$id.'][pickup]')) !!}
        {!! HTML::decode(Form::label('product_deliveries['.$id.'][pickup]', "Pick up", array('class' => $check_pickup?'checkbox marginright active':'checkbox marginright'))) !!}

        {!! Form::checkbox('product_deliveries['.$id.'][delivery]', 1, $check_delivery, array('class' => 'form-checkbox' , 'id' => 'product_deliveries['.$id.'][delivery]')) !!}
        {!! HTML::decode(Form::label('product_deliveries['.$id.'][delivery]', "Delivery", array('class' => $check_delivery?'checkbox marginright active':'checkbox marginright'))) !!}

        <select name="product_deliveries[{{$id}}][message]" class="form-control">
            <option value="0" {{$productDelivery->message == 0 ? 'selected' : ''}}">Product in stock</option>
            <option value="1" {{$productDelivery->message == 1 ? 'selected' : ''}}>Product out of stock (no orders possible)</option>
            <option value="2" {{$productDelivery->message == 2 ? 'selected' : ''}}>Soil temperature to low (no orders possible)</option>
            <option value="3" {{$productDelivery->message == 3 ? 'selected' : ''}}>Soil is too dry</option>
            <option value="4" {{$productDelivery->message == 4 ? 'selected' : ''}}>Product out of stock - replace with Heteri Guard</option>
            <option value="5" {{$productDelivery->message == 5 ? 'selected' : ''}}>Product out of stock - replace with Bio anti-slak</option>
        </select>

    </td>
    <td>
        <button class="btn btn-default pull-right remove-product-delivery-row" data-id="{{$id}}" type="button"><i class="far fa-trash-alt"></i></button>
    </td>
</tr>