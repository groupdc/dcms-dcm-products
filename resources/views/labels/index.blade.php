@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
      <h1>Labels</h1>
      <ol class="breadcrumb">
        <li><a href="{!! URL::to('admin/dashboard') !!}"><i class="far fa-tachometer-alt-average"></i> Dashboard</a></li>
        <li><a href="{!! URL::to('admin/products') !!}"><i class="far fa-boxes"></i> Products</a></li>
        <li class="active"><i class="far fa-tags"></i> Labels</li>
      </ol>
    </div>

    <div class="main-content">
    	<div class="row">
				<div class="col-md-12">
					<div class="main-content-block">

  @if (Session::has('message'))
    <div class="alert alert-info">{!! Session::get('message') !!}</div>
  @endif

    <div class="btnbar btnbar-right"><a class="btn btn-small btn-primary" href="{!! URL::to('/admin/labels/create') !!}">Create new</a></div>

    <h2>Overview</h2>

        <table id="datatable" class="table table-hover table-condensed" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Image</th>
                    <th>Country</th>
                    <th></th>
                </tr>
            </thead>
        </table>

        <script type="text/javascript">
            $(document).ready(function() {
                oTable = $('#datatable').DataTable({
                    "pageLength": 50,
                    "processing": true,
                    "serverSide": true,
                    "ajax": "{{ route('admin.labels.api.table') }}",
                    "columns": [
                        {data: 'id', name: 'id'},
                        {data: 'title', name: 'title'},
                        {data: 'image', name: 'image', searchable: false},
                        {data: 'regio', name: 'regio', searchable: false},
                        {data: 'edit', name: 'edit', orderable: false, searchable: false}
                    ]
                });
            });
        </script>

        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">

        <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>

	      	</div>
      	</div>
      </div>
    </div>

@stop
