@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
      <h1>Labels</h1>
      <ol class="breadcrumb">
        <li><a href="{!! URL::to('admin/dashboard') !!}"><i class="far fa-tachometer-alt-average"></i> Dashboard</a></li>
        <li><a href="{!! URL::to('admin/products') !!}"><i class="far fa-boxes"></i> Products</a></li>
        <li><a href="{!! URL::to('admin/labels') !!}"><i class="far fa-tags"></i> Labels</a></li>
        @if(isset($Label))
            <li class="active"><i class="far fa-pencil"></i> Label {{ $Label->id }}</li>
        @else
            <li class="active"><i class="far fa-plus-circle"></i> Create Label</li>
        @endif
      </ol>
    </div>

    <div class="main-content">

    @if(isset($Label))
        {!! Form::model($Label, array('route' => array('admin.labels.update', $Label->id), 'method' => 'PUT')) !!}
    @else
        {!! Form::open(array('url' => 'admin/labels')) !!}
    @endif

    	<div class="row">
				<div class="col-md-12">
					<div class="main-content-block">

              @if($errors->any())
                <div class="alert alert-danger">{!! HTML::ul($errors->all()) !!}</div>
              @endif

              <div class="form-group">
                {!! Form::label('sort_id', 'Sort') !!}
                {!! Form::text('sort_id', (old('sort_id') ? old('sort_id') : (isset($Label)?$Label->sort_id:'')), array('class' => 'form-control')) !!}
              </div>
              
              <!-- #FILTER-->
              <div class="form-group">
                  {!! Form::checkbox('filter', '1', null, array('class' => 'form-checkbox','id'=>'filter'))  !!}
                  {!! Html::decode(Form::label('filter', 'Filter', array('class' => (isset($Label) && $Label->filter==1)?'checkbox active':'checkbox'))) !!}
              </div>
              
              <!-- #TYPE-->
              <div class="form-group">
                  <label for="labeltype">Type</label>
                  <select id="labeltype" name="label" class="form-control">
                      <option value="hobby" {{ (isset($Label) && $Label->type=="hobby")?'selected':'' }}>Hobby</option>
                      <option value="pro" {{ (isset($Label) && $Label->type=="pro")?'selected':'' }}>Pro</option>
                  </select>
              </div>

              @if(isset($languages))
                  <ul class="nav nav-tabs" role="tablist">
                    @foreach($languages as $key => $language)
                          <li class="{!! ((intval(session('overrule_default_by_language_id')) == intval($language->language_id)) ? 'active' : ((intval(session('overrule_default_by_language_id')) == 0 && $key == 0) ? 'active': '') ) !!}"><a href="{!! '#' . $language->language . '-' . $language->country !!}" role="tab" data-toggle="tab"><img src="{!! asset('/packages/Dcms/Core/images/flag-' . strtolower($language->country) . '.svg') !!}" width="16" height="16" /> {!! $language->language_name !!}</a></li>
                    @endforeach
                  </ul>

                  <div class="tab-content">
              @foreach($languages as $key => $information)

                    <div id="{!! $information->language . '-' . $information->country !!}" class="tab-pane {!! ((intval(session('overrule_default_by_language_id')) == intval($information->language_id)) ? 'active' : ((intval(session('overrule_default_by_language_id')) == 0 && $key == 0) ? 'active': '')) !!}">

                      {!!Form::hidden('label_detail_id[' . $information->language_id . ']', $information->detail_id) !!}

                      <div class="form-group">
                        {!! Form::label('title[' . $information->language_id . ']', 'Title') !!}
                        {!! Form::text('title[' . $information->language_id . ']', (old('title[' . $information->language_id . ']') ? old('title[' . $information->language_id . ']') : $information->title ), array('class' => 'form-control')) !!}
                      </div>

                      <div class="form-group">
                        {!! Form::label('subtitle[' . $information->language_id . ']', 'Subtitle') !!}
                        {!! Form::text('subtitle[' . $information->language_id . ']', (old('subtitle[' . $information->language_id . ']') ? old('subtitle[' . $information->language_id . ']') : $information->subtitle ), array('class' => 'form-control')) !!}
                      </div>

                      <div class="form-group">
                        {!! Form::label('description[' . $information->language_id . ']', 'Description') !!}
                        {!! Form::textarea('description[' . $information->language_id . ']', (old('description[' . $information->language_id . ']') ? old('description[' . $information->language_id . ']') : $information->description ), array('class' => 'form-control')) !!}
                      </div>

                       <div class="form-group">
                          {!! Form::label('image'.$information->language_id, 'Image') !!}
                          <div class="input-group">
                              {!! Form::text('image['.$information->language_id.']', (old('image[' . $information->language_id . ']') ? old('image[' . $information->language_id . ']') : $information->image ) , array('class' => 'form-control' , 'id'=>'image-'.$information->language_id)) !!}
                            <span class="input-group-btn">
                              {!! Form::button('Browse Server', array('id'=>'browse_image-'.$information->language_id, 'class' => 'btn btn-primary browse-server')) !!}
                            </span>
                          </div>
                        </div>
                    </div>

              @endforeach
                  </div>

              @endif

                {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>

          </div>
        </div>
      </div>

      {!! Form::close() !!}

    </div>

@stop

@section("script")

<script type="text/javascript" src="{!! asset('/packages/Dcms/Core/js/bootstrap.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('/packages/Dcms/Core/js/bootstrap-datetimepicker.min.js') !!}"></script>
<link rel="stylesheet" type="text/css" href="{!! asset('/packages/Dcms/Core/css/bootstrap-datetimepicker.min.css') !!}">

<script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/ckeditor.js') !!}"></script>
<script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/adapters/jquery.js') !!}"></script>
<script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckfinder/ckfinder.js') !!}"></script>
<script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckfinder/ckbrowser.js') !!}"></script>

<script type="text/javascript">
$(document).ready(function() {

	//CKFinder for CKEditor
	CKFinder.setupCKEditor( null, '/ckfinder/' );


	//Browser --) $(this).attr("id")
	$(".browse-server").click(function() {
		var returnid = $(this).attr("id").replace("browse_","") ;
		BrowseServer( 'Images:/', returnid);
	})

	//CKEditor
	$("textarea[id^='description']").ckeditor();
	$("textarea[id^='body']").ckeditor();

	//Bootstrap Tabs
	$(".tab-container .nav-tabs a").click(function (e) {
		e.preventDefault();
		$(this).tab('show');
	})

	//Bootstrap Datepicker
	$(".date").datetimepicker({
		todayHighlight: true,
		autoclose: true,
		pickerPosition: "bottom-left"
	});

});
</script>

@stop
