<?php

return [
    "Products" => [
        "icon"  => "fa-shopping-cart",
        "links" => [
            ["route" => "admin/labels", "label" => "Labels", "permission" => 'products'],
            ["route" => "admin/products", "label" => "Products", "permission" => 'products'],
            ["route" => "admin/products/information", "label" => "Information", "permission" => 'products'],
            ["route" => "admin/products/categories", "label" => "Categories", "permission" => 'products'],
        ],
    ],
];
